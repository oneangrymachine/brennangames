﻿using UnityEngine;
using Brennan.Games.Common.Collections;
using System.Collections.Generic;

namespace Brennan.Games.Lockstep.Unity
{
    public class UnityPlayerRegistry : IPlayerRegistry
    {
        private int expectedNumberOfPlayers;
        private IDictionary<string, NetworkPlayer> playerIDsToNetworkPlayers;

        public UnityPlayerRegistry(int numberOfPlayers)
        {
            expectedNumberOfPlayers = numberOfPlayers;
            playerIDsToNetworkPlayers = new OrderedDictionary<string, NetworkPlayer>(numberOfPlayers);
        }

        public void registerPlayer(NetworkPlayer player)
        {
            playerIDsToNetworkPlayers.Add(player.ToString(), player);
        }

        public bool isPlayerIDRegistered(string playerID)
        {
            return playerIDsToNetworkPlayers.ContainsKey(playerID);
        }

        public NetworkPlayer player(string playerID)
        {
            return playerIDsToNetworkPlayers[playerID];
        }

        public bool areAllPlayersRegistered()
        {
            return playerIDsToNetworkPlayers.Count >= expectedNumberOfPlayers;
        }

        public int registeredPlayerCount()
        {
            return playerIDsToNetworkPlayers.Count;
        }
    }
}

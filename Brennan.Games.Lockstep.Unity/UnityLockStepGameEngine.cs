﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Lockstep.Actions;
using Brennan.Games.Lockstep.Serialization;

namespace Brennan.Games.Lockstep.Unity
{
    public class UnityLockStepGameEngine : MonoBehaviour, IActionMessenger
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #region Unity injected dependencies (set in editor)
        public IGameStartBroadcast gameStartBroadcast;
        #endregion

        private bool gameStarted = false;
        private NetworkView nv;
        private IGameEngine gameEngine;
        private IPlayerRegistry playerRegistry;

        void Start()
        {
            enabled = false;

            nv = GetComponent<NetworkView>();

            gameStartBroadcast.PrepareGameEvent += PrepGameStart;
            gameStartBroadcast.GameStartServer += GameStartServer;
            gameStartBroadcast.GameStart += GameStart;
        }

        private void PrepGameStart(object sender, PrepareGameEventArgs eventArgs)
        {
            gameEngine = new GameEngine(eventArgs.NumberOfPlayers, eventArgs.MyPlayerID, this);
        }

        private void GameStartServer(object sender, GameStartServerEventArgs eventArgs)
        {
            playerRegistry = eventArgs.PlayerRegistry;
        }

        private void GameStart(object sender, EventArgs eventArgs)
        {
            //start the LockStep Turn loop
            enabled = true;
            gameStarted = true;
        }

        public void AddAction(PlayerAction action)
        {
            log.Debug("Action Added");
            if (!gameStarted)
            {
                log.Debug("Game has not started, action will be ignored.");
                return;
            }
            gameEngine.EnqueueActionToSend(action);
        }

        public void Update()
        {
            gameEngine.UpdateGame(Time.deltaTime);
        }

        #region Actions
        public void SendActionToAllOtherPlayers(LockStepTurn currentLockStepTurn, int sendingPlayerID, PlayerAction action)
        {
            nv.RPC("RecieveAction", RPCMode.Others, currentLockStepTurn.GetID(), sendingPlayerID.ToString(), BinarySerialization.SerializeObjectToByteArray(action));
        }

        [RPC]
        public void RecieveAction(int actionsLockStepTurnID, string playerID, byte[] actionAsBytes)
        {
            //log.Debug("Recieved Player " + playerID + "'s action for turn " + actionsLockStepTurn + " on our turn" + actionsManager.CurrentLockStepTurnID());
            LockStepTurn actionsLockStepTurn = new LockStepTurn(actionsLockStepTurnID);
            PlayerAction action = BinarySerialization.DeserializeObject<PlayerAction>(actionAsBytes);
            if (action == null)
            {
                //log.Debug("Recieved null action from player " + playerID + " for player's turn " + actionsLockStepTurn + "on our turn " + actionsManager.CurrentLockStepTurnID());
                //TODO: Error handle invalid actions recieve
            }
            else
            {
                gameEngine.RecievePlayersAction(action, Convert.ToInt32(playerID), actionsLockStepTurn);

                //send confirmation
                if (Network.isServer)
                {
                    //we don't need an rpc call if we are the server
                    ConfirmActionServer(actionsLockStepTurn.GetID(), Network.player.ToString(), playerID);
                }
                else
                {
                    nv.RPC("ConfirmActionServer", RPCMode.Server, actionsLockStepTurn, Network.player.ToString(), playerID);
                }
            }
        }

        [RPC]
        public void ConfirmActionServer(int lockStepTurnID, string confirmingPlayerID, string confirmedPlayerID)
        {
            if (!Network.isServer) { return; } //Workaround - if server and client on same machine

            log.Debug("ConfirmActionServer called for turn" + lockStepTurnID + " and playerID " + confirmingPlayerID);
            log.Debug("Sending Confirmation to player " + confirmedPlayerID);

            if (Network.player.ToString().Equals(confirmedPlayerID))
            {
                //we don't need an RPC call if this is the server
                ConfirmAction(lockStepTurnID, confirmingPlayerID);
            }
            else
            {
                nv.RPC("ConfirmAction", playerRegistry.player(confirmedPlayerID), lockStepTurnID, confirmingPlayerID);
            }
        }

        [RPC]
        public void ConfirmAction(int confirmedActionLockStepTurnID, string confirmingPlayerID)
        {
            LockStepTurn confirmedActionLockStepTurn = new LockStepTurn(confirmedActionLockStepTurnID);
            gameEngine.ConfirmActionRecieved(Convert.ToInt32(confirmingPlayerID), confirmedActionLockStepTurn);
        }
        #endregion
    }
}

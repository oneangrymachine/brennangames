﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Brennan.Games.Lockstep
{
    public interface IGameStartBroadcast
    {
        /// <summary>
        /// Will be raised before the game starts. Notifies any listeners 
        /// of the number of players for the match.
        /// 
        /// Note that this method is guaranteed to have been executed on
        /// every player's client. However GameStart may be called on
        /// another player's client before this one, so any necassary
        /// setup to handle other player's requests should be triggered
        /// from this event.
        /// </summary>
        event EventHandler<PrepareGameEventArgs> PrepareGameEvent;

        /// <summary>
        /// Will be raised on the host player only. Supplies listeners
        /// with a reference to the PlayerRegistry.
        /// </summary>
        event EventHandler<GameStartServerEventArgs> GameStartServer;

        /// <summary>
        /// Will be raised once the game is ready to start. Any setup
        /// required by listeners should have taken place already in
        /// the PrepareGame event.
        /// </summary>
        event EventHandler GameStart;
    }

    public class PrepareGameEventArgs : EventArgs
    {
        public PrepareGameEventArgs(int numberOfPlayers, int myPlayerID)
        {
            NumberOfPlayers = numberOfPlayers;
            MyPlayerID = myPlayerID;
        }

        public int NumberOfPlayers { get; private set; }
        public int MyPlayerID { get; private set; }
    }

    public class GameStartServerEventArgs : EventArgs
    {
        public GameStartServerEventArgs(IPlayerRegistry playerRegistry)
        {
            PlayerRegistry = playerRegistry;
        }

        public IPlayerRegistry PlayerRegistry { get; private set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Brennan.Games.Lockstep
{
    public interface IPlayerRegistry
    {
        NetworkPlayer player(String playerID);
    }
}

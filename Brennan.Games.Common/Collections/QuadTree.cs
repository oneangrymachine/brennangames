﻿using System;
using System.Collections.Generic;
using Brennan.Games.Common.Numeric;
using Brennan.Games.Common.Collections;

namespace Brennan.Games.Common.Collections
{
    internal class QuadTree
    {
        internal int MaxDepth { get; private set; }
        internal int MaxObjects { get; private set; }

        private QuadTreeNode rootNode;

        internal QuadTree(int maxDepth, int maxObjects, Rectangle bounds)
        {
            MaxDepth = maxDepth;
            MaxObjects = maxObjects;
            rootNode = new QuadTreeNode(0, bounds, this);
        }

        internal void Insert(PhysicsObject obj)
        {
            rootNode.Insert(obj);
        }

        internal void Remove(PhysicsObject obj)
        {
            rootNode.Remove(obj);
        }

        internal List<PhysicsObject> GetCollisions(PhysicsObject obj)
        {
            return rootNode.GetCollisions(obj.Bounds);
        }

        internal List<PhysicsObject> GetCollisions(Rectangle rect)
        {
            return rootNode.GetCollisions(rect);
        }

        internal OrderedSet<CollisionBroadPhase> GetAllCollisions()
        {
            return rootNode.GetAllCollisions();   
        }

        class QuadTreeNode
        {
            int Depth { get; set; }

            QuadTree tree;
            Rectangle myBounds;
            QuadTreeNode[] children;
            List<PhysicsObject> myObjects;

            const int BottomLeft = 0;
            const int BottomRight = 1;
            const int TopLeft = 2;
            const int TopRight = 3;

            internal QuadTreeNode(int depth, Rectangle bounds, QuadTree tree)
            {
                Depth = depth;
                this.tree = tree;
                this.myBounds = bounds;
                myObjects = new List<PhysicsObject>();
            }

            internal void Insert(PhysicsObject obj)
            {
                //only continue to add if we intersect with the object
                if (!myBounds.Intersect(obj.Bounds)) return;

                //check if we have children
                if (children != null)
                {
                    //add obj to each child that intersects it
                    AddToChildren(obj);
                    return;
                }

                //check if we reached max objects, and that we have not reached the max depth
                if (myObjects.Count >= tree.MaxObjects && Depth < tree.MaxDepth)
                {
                    //create children
                    children = new QuadTreeNode[4];

                    //bottom left quadrant
                    children[BottomLeft] = new QuadTreeNode(Depth + 1, new Rectangle(myBounds.Min + (myBounds.ExtentsMin / 2), myBounds.ExtentsMin), tree);

                    //top right quadrant
                    children[TopRight] = new QuadTreeNode(Depth + 1, new Rectangle(myBounds.Max - (myBounds.ExtentsMax.DivRoundUp(2)), myBounds.ExtentsMax), tree);

                    //bottom right quadrant
                    children[BottomRight] = new QuadTreeNode(
                        Depth + 1,
                        new Rectangle(new Vector2Int(
                                        children[TopRight].myBounds.Center.X,
                                        children[BottomLeft].myBounds.Center.Y),
                                      new Vector2Int(
                                        children[TopRight].myBounds.Size.X,
                                        children[BottomLeft].myBounds.Size.Y
                                          )),
                        tree);

                    //top left quadrant
                    children[TopLeft] = new QuadTreeNode(
                        Depth + 1,
                        new Rectangle(new Vector2Int(
                                        children[BottomLeft].myBounds.Center.X,
                                        children[TopRight].myBounds.Center.Y),
                                      new Vector2Int(
                                        children[BottomLeft].myBounds.Size.X,
                                        children[TopRight].myBounds.Size.Y
                                          )),
                        tree);
                    
                    while (myObjects.Count > 0)
                    {
                        AddToChildren(myObjects[myObjects.Count - 1]);
                        myObjects.RemoveAt(myObjects.Count - 1);
                    }

                    AddToChildren(obj);
                    return;
                }

                //we are a leaf node, and we have not reached max capacity, add here
                myObjects.Add(obj);
            }

            void AddToChildren(PhysicsObject obj)
            {
                for (int index = 0; index < 4; index++)
                {
                    children[index].Insert(obj);
                }
            }

            internal void Remove(PhysicsObject obj)
            {
                //only need to continue if we intersect with the object
                if (!myBounds.Intersect(obj.Bounds)) return;

                //if we have children, remove from them
                if (children != null)
                {
                    for (int index = 0; index < 4; index++)
                    {
                        children[index].Remove(obj);
                    }
                    return;
                }

                //otherwise remove from our list
                myObjects.Remove(obj);
            }

            internal List<PhysicsObject> GetCollisions(Rectangle r)
            {
                List<PhysicsObject> collisions = new List<PhysicsObject>();
                GetCollisions(r, collisions);
                return collisions;
            }

            void GetCollisions(Rectangle r, List<PhysicsObject> collisions) 
            {
                //we only need to continue if we intersect the object
                if(!myBounds.Intersect(r)) return;

                //if we have children, check collisions with them
                if (children != null)
                {
                    for (int index = 0; index < 4; index++)
                    {
                        children[index].GetCollisions(r, collisions);
                    }
                }
                else
                {
                    //otherwise check collisions with our objects
                    foreach (PhysicsObject o in myObjects)
                    {
                        if (r.Intersect(o.Bounds))
                        {
                            collisions.Add(o);
                        }
                    }
                }
            }

            internal OrderedSet<CollisionBroadPhase> GetAllCollisions()
            {
                OrderedSet<CollisionBroadPhase> collisions = new OrderedSet<CollisionBroadPhase>();
                GetAllCollisions(collisions);
                return collisions;
            }

            void GetAllCollisions(OrderedSet<CollisionBroadPhase> collisionSet)
            {
                if (children != null)
                {
                    for (int index = 0; index < 4; index++)
                    {
                        children[index].GetAllCollisions(collisionSet);
                    }
                }
                else
                {
                    //this is a leaf node, get all collisions at this level and insert into the set
                    for (int index = 0; index < myObjects.Count - 1; index++)
                    {
                        for (int otherIndex = index + 1; otherIndex < myObjects.Count; otherIndex++)
                        {
                            if (myObjects[index].Bounds.Intersect(myObjects[otherIndex].Bounds))
                            {
                                collisionSet.Add(new CollisionBroadPhase(myObjects[index], myObjects[otherIndex]));
                            }
                        }
                    }
                }
            }
        }
    }

   
}

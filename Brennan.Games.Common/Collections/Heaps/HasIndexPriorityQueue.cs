﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Brennan.Games.Common.Collections.Heaps
{
    public class HasIndexPriorityQueue<T> : PriorityQueue<T> where T : HasIndex, IComparable
    {
        protected override void setIndex(T point, int index)
        {
            point.Index = index;
        }

        protected override int getIndex(T point)
        {
            return point.Index;
        }

        protected override void clearIndex(T point)
        {
            point.Index = -1;
        }

        protected override int Compare(int i, int j)
        {
            return innerList[i].CompareTo(innerList[j]);
        }
    }
}
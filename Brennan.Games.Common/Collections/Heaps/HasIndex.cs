﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Brennan.Games.Common.Collections.Heaps
{
    /// <summary>
    /// Value Type classes used with the PriorityQueue can implement
    /// this interface to increase the runtime of the PriorityQueue's operations.
    /// </summary>
    public interface HasIndex
    {
        int Index { get; set; }
    }
}

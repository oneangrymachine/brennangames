﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;


namespace Brennan.Games.Common.Collections.Heaps
{
    public class PrioritySet<T> : PriorityQueue<T>
    {
        public PrioritySet() : base() { }

        public PrioritySet(IComparer<T> comparer) : base(comparer) { }

        private OrderedSet<T> set = new OrderedSet<T>();

        public override void Push(T entry)
        {
            if (!set.Contains(entry))
            {
                set.Add(entry);
                base.Push(entry);
            }
        }

        public override T Pop()
        {
            T entry = base.Pop();
            set.Remove(entry);
            return entry;
        }

        new public bool Contains(T entry)
        {
            return set.Contains(entry);
        }
    }
}

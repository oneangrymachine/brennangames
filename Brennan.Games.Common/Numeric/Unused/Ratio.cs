﻿using System;
using Sys;

namespace Brennan.Games.Common.Numeric.Unused
{

    public struct Ratio : IComparable<Ratio>
    {

        private int numerator;
        public int Numerator { get { return numerator; } }

        private int denominator;
        public int Denominator { get { return denominator; } }

        public Ratio(int numerator, int denominator)
        {
            if (denominator == 0)
            {
                Console.WriteLine("Test");
            }
            int gcd = GCD(numerator, denominator);

            if (gcd != 0)
            {
                if (numerator > 10000 && denominator > 10000)
                {
                    gcd = gcd * 1000;
                }

                this.numerator = numerator / gcd;
                this.denominator = denominator / gcd;
            }
            else
            {
                this.numerator = numerator;
                this.denominator = denominator;
            }

            
        }

        public Ratio(int numerator)
        {
            this.numerator = numerator;
            this.denominator = 1;
        }

        #region Ratio Ratio Operators
        public static Ratio operator +(Ratio a, Ratio b)
        {
            return new Ratio(
                            (a.Numerator * b.Denominator) + (b.Numerator * a.Denominator), //Numerator
                            a.Denominator * b.Denominator //Denominator
                            );
        }

        public static Ratio operator -(Ratio a, Ratio b)
        {
            return new Ratio(
                            (a.Numerator * b.Denominator) - (b.Numerator * a.Denominator), //Numerator
                            a.Denominator * b.Denominator //Denominator
                            );
        }

        public static Ratio operator /(Ratio a, Ratio b)
        {
            return new Ratio(
                             a.Numerator * b.Denominator, //Numerator
                             b.Numerator * a.Denominator  //Denominator
                             );
        }

        public static Ratio operator *(Ratio a, Ratio b)
        {
            return new Ratio(
                            a.Numerator * b.Numerator, //Numerator
                            a.Denominator * b.Denominator //Denominator
                            );
        }

        public static bool operator >(Ratio a, Ratio b)
        {
            return (a.Numerator * b.Denominator) > (b.Numerator * a.Denominator);
        }

        public static bool operator >=(Ratio a, Ratio b)
        {
            return (a.Numerator * b.Denominator) >= (b.Numerator * a.Denominator);
        }

        public static bool operator <(Ratio a, Ratio b)
        {
            return (a.Numerator * b.Denominator) < (b.Numerator * a.Denominator);
        }

        public static bool operator <=(Ratio a, Ratio b)
        {
            return (a.Numerator * b.Denominator) <= (b.Numerator * a.Denominator);
        }

        public static bool operator ==(Ratio a, Ratio b)
        {
            return (a.Numerator * b.Denominator) == (b.Numerator * a.Denominator);
        }

        public static bool operator !=(Ratio a, Ratio b)
        {
            return (a.Numerator * b.Denominator) != (b.Numerator * a.Denominator);
        }
        #endregion Ratio Ratio operators

        #region Ratio Int Operators
        public static Ratio operator -(Ratio a, int i)
        {
            return new Ratio(
                            (a.Numerator - i * a.Denominator), //Numerator
                            a.Denominator                      //Denominator
                            );
        }

        public static Ratio operator -(int i, Ratio a)
        {
            return new Ratio(
                            (i * a.Denominator - a.Numerator), //Numerator
                            a.Denominator
                            );
        }

        public static Ratio operator +(Ratio a, int i)
        {
            return new Ratio(
                            (a.Numerator + i * a.Denominator), //Numerator
                            a.Denominator
                            );
        }

        public static Ratio operator +(int i, Ratio a)
        {
            return a + i;
        }

        public static Ratio operator *(Ratio a, int i)
        {
            return new Ratio(
                             a.Numerator * i,
                             a.Denominator
                             );
        }

        public static Ratio operator *(int i, Ratio a)
        {
            return a * i;
        }

        public static Ratio operator /(Ratio a, int i)
        {
            return new Ratio(
                             a.Numerator, //Numerator
                             a.Denominator * i //Denominator
                             );
        }

        public static Ratio operator /(int i, Ratio a)
        {
            return new Ratio(
                             a.Denominator * i, //Numerator
                             a.Numerator        //Denominator
                             );
        }

        public static bool operator >(Ratio a, int b)
        {
            return (a.Numerator) > (b * a.Denominator);
        }

        public static bool operator >=(Ratio a, int b)
        {
            return (a.Numerator) >= (b * a.Denominator);
        }

        public static bool operator <(Ratio a, int b)
        {
            return (a.Numerator) < (b * a.Denominator);
        }

        public static bool operator <=(Ratio a, int b)
        {
            return (a.Numerator) <= (b * a.Denominator);
        }

        public static bool operator ==(Ratio a, int b)
        {
            return (a.Numerator) == (b * a.Denominator);
        }

        public static bool operator !=(Ratio a, int b)
        {
            return (a.Numerator) != (b * a.Denominator);
        }

        public static implicit operator Ratio(int a)
        {
            return new Ratio(a);
        }
        #endregion Ratio Int operators

        public int Resolve()
        {
            if (denominator == 0) { return 0; }

            return Numerator / Denominator;
        }

        public Ratio Reduce()
        {
            int gcd = GCD(Numerator, Denominator);
            return new Ratio(Numerator / gcd, Denominator / gcd);
        }

        public Ratio Clamp(Ratio min, Ratio max)
        {
            if (this <= min) { return min; }
            if (this >= max) { return max; }
            return this;
        }

        #region Static Methods
        public static Ratio Min(Ratio a, Ratio b)
        {
            if (a < b)
            {
                return a;
            }
            else
            {
                return b;
            }
        }

        public static int GCD(int a, int b)
        {
            if (b == 0)
            {
                return a;
            }
            return GCD(b, a % b);
        }

        public static Ratio Abs(Ratio a)
        {
            return new Ratio(Math.Abs(a.numerator), Math.Abs(a.denominator));
        }
        #endregion Static Methods

        public int CompareTo(Ratio other)
        {
            return (Numerator * other.Denominator) - (other.Numerator * Denominator);
        }

        public override bool Equals(object obj)
        {
            if (obj is Ratio)
            {
                Ratio other = (Ratio)obj;
                if (this == other)
                {
                    return true;
                }
            }

            return false;
        }

        public override int GetHashCode()
        {
            Ratio r = Reduce();
            return (new Tuple<int, int>(r.Numerator, r.Denominator)).GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("{0}/{1}", numerator, denominator);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Brennan.Games.Common.Numeric
{
    public class Trig
    {
        const int Scale = 100;
        static int arcCosMin = -1 * Scale;
        //static int arcCosMax = 1 * Scale;

        #region arcCosLookup
        static int[] arcCosLookup = {
                                    18000, //arcCos(-1) times 100
                                    17189, //arcCos(-0.99) times 100
                                    16852, //arcCos(-0.98) times 100
                                    16593, //arcCos(-0.97) times 100
                                    16373, //arcCos(-0.96) times 100
                                    16180, //arcCos(-0.95) times 100
                                    16005, //arcCos(-0.94) times 100
                                    15843, //arcCos(-0.93) times 100
                                    15692, //arcCos(-0.92) times 100
                                    15550, //arcCos(-0.91) times 100
                                    15415, //arcCos(-0.9) times 100
                                    15287, //arcCos(-0.89) times 100
                                    15164, //arcCos(-0.88) times 100
                                    15045, //arcCos(-0.87) times 100
                                    14931, //arcCos(-0.86) times 100
                                    14821, //arcCos(-0.85) times 100
                                    14714, //arcCos(-0.84) times 100
                                    14609, //arcCos(-0.83) times 100
                                    14508, //arcCos(-0.82) times 100
                                    14409, //arcCos(-0.81) times 100
                                    14313, //arcCos(-0.8) times 100
                                    14218, //arcCos(-0.79) times 100
                                    14126, //arcCos(-0.78) times 100
                                    14035, //arcCos(-0.77) times 100
                                    13946, //arcCos(-0.76) times 100
                                    13859, //arcCos(-0.75) times 100
                                    13773, //arcCos(-0.74) times 100
                                    13688, //arcCos(-0.73) times 100
                                    13605, //arcCos(-0.72) times 100
                                    13523, //arcCos(-0.71) times 100
                                    13442, //arcCos(-0.7) times 100
                                    13363, //arcCos(-0.69) times 100
                                    13284, //arcCos(-0.68) times 100
                                    13206, //arcCos(-0.67) times 100
                                    13129, //arcCos(-0.66) times 100
                                    13054, //arcCos(-0.65) times 100
                                    12979, //arcCos(-0.64) times 100
                                    12905, //arcCos(-0.63) times 100
                                    12831, //arcCos(-0.62) times 100
                                    12758, //arcCos(-0.61) times 100
                                    12686, //arcCos(-0.6) times 100
                                    12615, //arcCos(-0.59) times 100
                                    12545, //arcCos(-0.58) times 100
                                    12475, //arcCos(-0.57) times 100
                                    12405, //arcCos(-0.56) times 100
                                    12336, //arcCos(-0.55) times 100
                                    12268, //arcCos(-0.54) times 100
                                    12200, //arcCos(-0.53) times 100
                                    12133, //arcCos(-0.52) times 100
                                    12066, //arcCos(-0.51) times 100
                                    12000, //arcCos(-0.5) times 100
                                    11934, //arcCos(-0.49) times 100
                                    11868, //arcCos(-0.48) times 100
                                    11803, //arcCos(-0.47) times 100
                                    11738, //arcCos(-0.46) times 100
                                    11674, //arcCos(-0.45) times 100
                                    11610, //arcCos(-0.44) times 100
                                    11546, //arcCos(-0.43) times 100
                                    11483, //arcCos(-0.42) times 100
                                    11420, //arcCos(-0.41) times 100
                                    11357, //arcCos(-0.4) times 100
                                    11295, //arcCos(-0.39) times 100
                                    11233, //arcCos(-0.38) times 100
                                    11171, //arcCos(-0.37) times 100
                                    11110, //arcCos(-0.36) times 100
                                    11048, //arcCos(-0.35) times 100
                                    10987, //arcCos(-0.34) times 100
                                    10926, //arcCos(-0.33) times 100
                                    10866, //arcCos(-0.32) times 100
                                    10805, //arcCos(-0.31) times 100
                                    10745, //arcCos(-0.3) times 100
                                    10685, //arcCos(-0.29) times 100
                                    10626, //arcCos(-0.28) times 100
                                    10566, //arcCos(-0.27) times 100
                                    10507, //arcCos(-0.26) times 100
                                    10447, //arcCos(-0.25) times 100
                                    10388, //arcCos(-0.24) times 100
                                    10329, //arcCos(-0.23) times 100
                                    10270, //arcCos(-0.22) times 100
                                    10212, //arcCos(-0.21) times 100
                                    10153, //arcCos(-0.2) times 100
                                    10095, //arcCos(-0.19) times 100
                                    10036, //arcCos(-0.18) times 100
                                    9978, //arcCos(-0.17) times 100
                                    9920, //arcCos(-0.16) times 100
                                    9862, //arcCos(-0.15) times 100
                                    9804, //arcCos(-0.14) times 100
                                    9746, //arcCos(-0.13) times 100
                                    9689, //arcCos(-0.12) times 100
                                    9631, //arcCos(-0.11) times 100
                                    9573, //arcCos(-0.1) times 100
                                    9516, //arcCos(-0.09) times 100
                                    9458, //arcCos(-0.08) times 100
                                    9401, //arcCos(-0.07) times 100
                                    9343, //arcCos(-0.06) times 100
                                    9286, //arcCos(-0.05) times 100
                                    9229, //arcCos(-0.04) times 100
                                    9171, //arcCos(-0.03) times 100
                                    9114, //arcCos(-0.02) times 100
                                    9057, //arcCos(-0.01) times 100
                                    9000, //arcCos(0) times 100
                                    8942, //arcCos(0.01) times 100
                                    8885, //arcCos(0.02) times 100
                                    8828, //arcCos(0.03) times 100
                                    8770, //arcCos(0.04) times 100
                                    8713, //arcCos(0.05) times 100
                                    8656, //arcCos(0.06) times 100
                                    8598, //arcCos(0.07) times 100
                                    8541, //arcCos(0.08) times 100
                                    8483, //arcCos(0.09) times 100
                                    8426, //arcCos(0.1) times 100
                                    8368, //arcCos(0.11) times 100
                                    8310, //arcCos(0.12) times 100
                                    8253, //arcCos(0.13) times 100
                                    8195, //arcCos(0.14) times 100
                                    8137, //arcCos(0.15) times 100
                                    8079, //arcCos(0.16) times 100
                                    8021, //arcCos(0.17) times 100
                                    7963, //arcCos(0.18) times 100
                                    7904, //arcCos(0.19) times 100
                                    7846, //arcCos(0.2) times 100
                                    7787, //arcCos(0.21) times 100
                                    7729, //arcCos(0.22) times 100
                                    7670, //arcCos(0.23) times 100
                                    7611, //arcCos(0.24) times 100
                                    7552, //arcCos(0.25) times 100
                                    7492, //arcCos(0.26) times 100
                                    7433, //arcCos(0.27) times 100
                                    7373, //arcCos(0.28) times 100
                                    7314, //arcCos(0.29) times 100
                                    7254, //arcCos(0.3) times 100
                                    7194, //arcCos(0.31) times 100
                                    7133, //arcCos(0.32) times 100
                                    7073, //arcCos(0.33) times 100
                                    7012, //arcCos(0.34) times 100
                                    6951, //arcCos(0.35) times 100
                                    6889, //arcCos(0.36) times 100
                                    6828, //arcCos(0.37) times 100
                                    6766, //arcCos(0.38) times 100
                                    6704, //arcCos(0.39) times 100
                                    6642, //arcCos(0.4) times 100
                                    6579, //arcCos(0.41) times 100
                                    6516, //arcCos(0.42) times 100
                                    6453, //arcCos(0.43) times 100
                                    6389, //arcCos(0.44) times 100
                                    6325, //arcCos(0.45) times 100
                                    6261, //arcCos(0.46) times 100
                                    6196, //arcCos(0.47) times 100
                                    6131, //arcCos(0.48) times 100
                                    6065, //arcCos(0.49) times 100
                                    6000, //arcCos(0.5) times 100
                                    5933, //arcCos(0.51) times 100
                                    5866, //arcCos(0.52) times 100
                                    5799, //arcCos(0.53) times 100
                                    5731, //arcCos(0.54) times 100
                                    5663, //arcCos(0.55) times 100
                                    5594, //arcCos(0.56) times 100
                                    5524, //arcCos(0.57) times 100
                                    5454, //arcCos(0.58) times 100
                                    5384, //arcCos(0.59) times 100
                                    5313, //arcCos(0.6) times 100
                                    5241, //arcCos(0.61) times 100
                                    5168, //arcCos(0.62) times 100
                                    5094, //arcCos(0.63) times 100
                                    5020, //arcCos(0.64) times 100
                                    4945, //arcCos(0.65) times 100
                                    4870, //arcCos(0.66) times 100
                                    4793, //arcCos(0.67) times 100
                                    4715, //arcCos(0.68) times 100
                                    4636, //arcCos(0.69) times 100
                                    4557, //arcCos(0.7) times 100
                                    4476, //arcCos(0.71) times 100
                                    4394, //arcCos(0.72) times 100
                                    4311, //arcCos(0.73) times 100
                                    4226, //arcCos(0.74) times 100
                                    4140, //arcCos(0.75) times 100
                                    4053, //arcCos(0.76) times 100
                                    3964, //arcCos(0.77) times 100
                                    3873, //arcCos(0.78) times 100
                                    3781, //arcCos(0.79) times 100
                                    3686, //arcCos(0.8) times 100
                                    3590, //arcCos(0.81) times 100
                                    3491, //arcCos(0.82) times 100
                                    3390, //arcCos(0.83) times 100
                                    3285, //arcCos(0.84) times 100
                                    3178, //arcCos(0.85) times 100
                                    3068, //arcCos(0.86) times 100
                                    2954, //arcCos(0.87) times 100
                                    2835, //arcCos(0.88) times 100
                                    2712, //arcCos(0.89) times 100
                                    2584, //arcCos(0.9) times 100
                                    2449, //arcCos(0.91) times 100
                                    2307, //arcCos(0.92) times 100
                                    2156, //arcCos(0.93) times 100
                                    1994, //arcCos(0.94) times 100
                                    1819, //arcCos(0.95) times 100
                                    1626, //arcCos(0.96) times 100
                                    1406, //arcCos(0.97) times 100
                                    1147, //arcCos(0.98) times 100
                                    810, //arcCos(0.99) times 100
                                    0, //arcCos(1) times 100
                                    };
        #endregion arcCosLookup

        public static int GetArcCosScaled(int scaledVal)
        {
            //if(scaledVal < arcCosMin || scaledVal > arcCosMin) { return null };
            return arcCosLookup[scaledVal - arcCosMin];
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Common.Collections;

namespace Brennan.Games.Common.Numeric
{
    public class CardinalVector2Ints
    {
        private static IEnumerable<Vector2Int> cardinalDirections = new CardinalDirections();
        private static IEnumerable<Vector2Int> intercardinalDirections = new IntercardinalDirections();
        private static IEnumerable<Vector2Int> all = new ChainedEnumerable<Vector2Int>(cardinalDirections, intercardinalDirections);

        public static IEnumerable<Vector2Int> Cardinal
        {
            get
            {
                return cardinalDirections;
            }
        }

        public static IEnumerable<Vector2Int> Intercardinal
        {
            get
            {
                return intercardinalDirections;
            }
        }

        public static IEnumerable<Vector2Int> All
        {
            get
            {
                return all;
            }
        }

        class CardinalDirections : IEnumerable<Vector2Int>
        {

            public IEnumerator<Vector2Int> GetEnumerator()
            {
                yield return Vector2Int.Up;
                yield return Vector2Int.Right;
                yield return Vector2Int.Down;
                yield return Vector2Int.Left;
            }

            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }

        class IntercardinalDirections : IEnumerable<Vector2Int>
        {

            public IEnumerator<Vector2Int> GetEnumerator()
            {
                yield return Vector2Int.Up + Vector2Int.Right;
                yield return Vector2Int.Down + Vector2Int.Right;
                yield return Vector2Int.Down + Vector2Int.Left;
                yield return Vector2Int.Up + Vector2Int.Left;
            }

            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }
        }
    }
}

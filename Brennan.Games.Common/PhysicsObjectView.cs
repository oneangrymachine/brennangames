﻿using System;
using Brennan.Games.Common.Numeric;

namespace Brennan.Games.Common
{
    /// <summary>
    /// The public interface with game objects. Scales up/down the values by the GameWorldScale
    /// </summary>
    public class PhysicsObjectView
    {
        internal PhysicsObject gameObject;

        internal PhysicsObjectView(PhysicsObject gameObject)
        {
            this.gameObject = gameObject;
        }

        public ICollider Collider
        {
            get
            {
                return gameObject.Collider.ScaleDown(PhysicsEngine.GameWorldScale);
            }
        }

        public Vector2Int Velocity
        {
            get
            {
                return gameObject.Velocity / PhysicsEngine.GameWorldScale;
            }

            set
            {
                gameObject.Velocity = value * PhysicsEngine.GameWorldScale;
            }
        }

        public ColliderEnum ColliderType
        {
            get
            {
                return gameObject.ColliderType;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Common.Numeric;

namespace Brennan.Games.Common
{
    internal class CollisionResolver
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const int VelocityMagnify = 10;

        /// <summary>
        /// Resolves a collision. Based on article found at:
        /// http://gamedev.tutsplus.com/tutorials/implementation/create-custom-2d-physics-engine-aabb-circle-impulse-resolution/
        /// </summary>
        /// <param name="collision"></param>
        internal static void ResolveCollision(CollisionBroadPhase broadCollision)
        {
            CollisionNarrowPhase collision = CollisionNarrowPhase.GetNarrowCollision(broadCollision);
            if (collision == null) { return; }

            //log.Debug("Resolving collision A:" + collision.A.Center + " B:" + collision.B.Center);

            // Calculate relative velocity
            Vector2Int relativeVelocity = collision.B.Velocity - collision.A.Velocity;

            // Calculate relative velocity in terms of the normal direction
            int magnitudeAlongNormal = (relativeVelocity */*Dot Product*/ collision.CollisionNormal) / PhysicsEngine.GameWorldScale/*CollisionNormal was scaled up to avoid rounding to 0*/; 

            // Do not resolve if velocities are separating
            if (magnitudeAlongNormal > 0)
            {
                return;
            }

            // Calculate restitution
            int e = Math.Min(collision.A.RestitutionGWN, collision.B.RestitutionGWN);

            // Calculate impulse scalar
            int inverseMassSum = (collision.A.InverseMassGWN + collision.B.InverseMassGWN);

            int j;
            if (inverseMassSum != 0) //avoid dividing by 0, this might happen if two inmovable objects are colliding for some reason
            {
                j = ((-1) * ((1 * PhysicsEngine.GameWorldScale) + e) * magnitudeAlongNormal)
                          /
                          inverseMassSum;

                j = (j / PhysicsEngine.GameWorldScale) + (PhysicsEngine.GameWorldScale / 10)/*Add 1/10th to make sure objects with no initial velocity will still seperate*/;

                Vector2Int impulse = (j * collision.CollisionNormal) / PhysicsEngine.GameWorldScale/*CollisionNormal was scaled up to avoid rounding to 0*/;

                collision.A.Velocity = (collision.A.Velocity - collision.A.InverseMassGWN * impulse);
                collision.B.Velocity = (collision.B.Velocity + collision.B.InverseMassGWN * impulse);
            }
        }
    }
}

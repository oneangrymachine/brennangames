﻿using System;
using Brennan.Games.Common.Numeric;

namespace Brennan.Games.Common
{
    internal class CollisionNarrowPhase
    {
        internal PhysicsObject A { get; private set; }
        internal PhysicsObject B { get; private set; }

        private Vector2Int collisionNormal;
        internal Vector2Int CollisionNormal
        {
            get
            {
                return collisionNormal;
            }
        }

        private int penetration;
        internal int Penetration
        {
            get { return penetration; }
        }


        private CollisionNarrowPhase() { }

        internal static CollisionNarrowPhase GetNarrowCollision(CollisionBroadPhase broadCollision)
        {
            CollisionNarrowPhase narrowCollision = new CollisionNarrowPhase();


            if (broadCollision.A.Collider != null && broadCollision.B.Collider != null)
            {
                //determine collision type based on collider types
                //TODO: if more collider types are added, refactor to have a dictionary of type combinations to delegates
                //that perform the collision to avoid too many if statements

                if (broadCollision.A.Collider is Circle)
                {
                    if (broadCollision.B.Collider is Circle)
                    {
                        narrowCollision.A = broadCollision.A;
                        narrowCollision.B = broadCollision.B;
                        if (narrowCollision.CircleVsCirlce())
                        {
                            return narrowCollision;
                        }
                    }
                    else if (broadCollision.B.Collider is Rectangle)
                    {
                        narrowCollision.A = broadCollision.B; //Rectangle is expected to be A
                        narrowCollision.B = broadCollision.A; //Circle is expected to be B
                        if (narrowCollision.RectangleVsCircle())
                        {
                            return narrowCollision;
                        }
                    }
                }
                else if (broadCollision.A.Collider is Rectangle)
                {
                    if (broadCollision.B.Collider is Circle)
                    {
                        narrowCollision.A = broadCollision.A; //Rectangle is expected to be A
                        narrowCollision.B = broadCollision.B; //Circle is expected to be B
                        if (narrowCollision.RectangleVsCircle())
                        {
                            return narrowCollision;
                        }
                    }
                    else if (broadCollision.B.Collider is Rectangle)
                    {
                        narrowCollision.A = broadCollision.A;
                        narrowCollision.B = broadCollision.B;
                        if (narrowCollision.RectangleVsRectangle())
                        {
                            return narrowCollision;
                        }
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Returns if the two circles have collided. If they did collide,
        /// collisionNormal and penetration will be calculated.
        /// </summary>
        /// <param name="c1"></param>
        /// <param name="c2"></param>
        /// <returns></returns>
        private bool CircleVsCirlce()
        {
            Circle c1 = A.Collider as Circle;
            Circle c2 = B.Collider as Circle;
            collisionNormal = c2.Center - c1.Center;

            int radious = c1.Radious + c2.Radious;

            //avoid using Length until necassary as it uses Sqrt
            if (collisionNormal.LengthSquard > (radious*radious))
            {
                return false;
            }

            //circles have collided, continue calculation

            int d = collisionNormal.Length;

            // If distance between circles is not zero
            if (d != 0)
            {
                penetration = radious - d;

                //make the normal a unit vector (length of 1) * GameWorldScale
                collisionNormal = (collisionNormal * PhysicsEngine.GameWorldScale)/ d;

                return true;
            }
            else
            {
                //circles are on the same spot, pick any arbitrary direction
                penetration = c1.Radious;
                collisionNormal = (new Vector2Int(1, 0)) * PhysicsEngine.GameWorldScale;

                return true;
            }
        }

        /// <summary>
        /// Returns if the two rectangles have collided. If they did collide,
        /// collisionNormal and penetration will be calculated.
        /// </summary>
        /// <param name="r1"></param>
        /// <param name="r2"></param>
        /// <returns></returns>
        private bool RectangleVsRectangle()
        {
            Rectangle r1 = A.Collider as Rectangle;
            Rectangle r2 = B.Collider as Rectangle;

            collisionNormal = r2.Center - r1.Center;

            int xOverlap = r1.ExtentsMin.X + r2.ExtentsMin.X - Math.Abs(collisionNormal.X);

            //check collision on X
            if (xOverlap > 0)
            {
                int yOverlap = r1.ExtentsMin.Y + r2.ExtentsMin.Y - Math.Abs(collisionNormal.Y);

                //check collision on Y
                if (yOverlap > 0)
                {
                    //check for the axis of least penetration
                    if (xOverlap > yOverlap)
                    {
                        //y axis is least penetration
                        if (collisionNormal.Y < 0)
                        {
                            collisionNormal = new Vector2Int(0, -1);
                        }
                        else
                        {
                            collisionNormal = new Vector2Int(0, 1);
                        }
                        penetration = yOverlap;
                    }
                    else
                    {
                        //x axis is least penetration
                        if (collisionNormal.X < 0)
                        {
                            collisionNormal = new Vector2Int(-1, 0);
                        }
                        else
                        {
                            collisionNormal = new Vector2Int(1, 0);
                        }
                        penetration = xOverlap;
                    }

                    collisionNormal = collisionNormal * PhysicsEngine.GameWorldScale;

                    //overlapps on both x and y axis
                    return true;
                }

                //x axis overlaps, but y axis does not
                return false;
            }

            //does not overlap on x axis
            return false;
        }

        private bool RectangleVsCircle()
        {
            Rectangle r = A.Collider as Rectangle;
            Circle c = B.Collider as Circle;

            Vector2Int n = c.Center - r.Center;

            // Closest point on A to center of B, clamped by the rectangle bounds
            Vector2Int closest = r.ExtentsMin.Clamp(n);

            bool inside = false;

            // Circle is inside the AABB, so we need to clamp the circle's center
            // to the closest edge
            if (n == closest)
            {
                inside = true;

                // Clamp to closest extent
                // Find closest axis
                Vector2Int abs = n.Abs();
                if (abs.X > abs.Y)
                {
                    //x axis is shorter
                    if (closest.X > 0)
                    {
                        closest = closest.SetX(r.ExtentsMin.X);
                    }
                    else
                    {
                        closest = closest.SetX(r.ExtentsMin.X * -1);
                    }
                }
                else
                {
                    //y axis is shorter
                    if (closest.Y > 0)
                    {
                        closest = closest.SetY(r.ExtentsMin.Y);
                    }
                    else
                    {
                        closest = closest.SetY(r.ExtentsMin.Y * -1);
                    }
                }
            }

            collisionNormal = n - closest;

            int d = collisionNormal.LengthSquard;
            int radious = c.Radious;

            // Early out of the radius is shorter than distance to closest point and
            // Circle not inside the AABB
            if (d > (radious * radious) && !inside)
            {
                return false;
            }

            d = collisionNormal.Length;

            if (d == 0) { d = 1; }

            // Collision normal needs to be flipped to point outside if circle was
            // inside the AABB
            if (inside)
            {
                collisionNormal = collisionNormal * -1;
            }

            collisionNormal = (collisionNormal * PhysicsEngine.GameWorldScale) / d;

            penetration = radious + d;

            return true;
        }
    }
}

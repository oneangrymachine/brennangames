﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Common.Numeric;

namespace Brennan.Games.Common
{
    public interface ICollider
    {
        Vector2Int Center { get; set; }

        ICollider ScaleUp(int scale);

        ICollider ScaleDown(int scale);
    }
}

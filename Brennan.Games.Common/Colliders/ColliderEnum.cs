﻿using System;

namespace Brennan.Games.Common
{
    public enum ColliderEnum
    {
        Circle,
        Rectangle
    }
}

﻿using System;
using Sys;

namespace Brennan.Games.Common
{
    /// <summary>
    /// Represents a collision determined during the broad phase check.
    /// This collision is based on the AABB only of the two GameObjects.
    /// </summary>
    internal class CollisionBroadPhase
    {
        public PhysicsObject A { get; private set; }
        public PhysicsObject B { get; private set; }

        private int hash;

        internal CollisionBroadPhase(PhysicsObject a, PhysicsObject b)
        {
            A = a;
            B = b;

            //make sure objects are always in the same order when calculating hash
            if (A.CompareTo(B) < 0)
            {
                hash = new Tuple<PhysicsObject, PhysicsObject>(A, B).GetHashCode();
            }
            else
            {
                hash = new Tuple<PhysicsObject, PhysicsObject>(B, A).GetHashCode();
            }
        }

        public override int GetHashCode()
        {
            return hash;
        }

        public override bool Equals(object obj)
        {
            if (obj is CollisionBroadPhase)
            {
                CollisionBroadPhase other = (CollisionBroadPhase)obj;
                if((A.Equals(other.A) && B.Equals(other.B)) || (A.Equals(other.B) && B.Equals(other.A))) {
                    return true;
                }
            }

            return false;
        }
    }
}

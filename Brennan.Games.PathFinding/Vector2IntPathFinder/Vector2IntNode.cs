﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Common.Numeric;
using Brennan.Games.PathFinding.AStar;
using Brennan.Games.Common.Collections;
using Sys;

namespace Brennan.Games.PathFinding.Vector2IntPathFinder
{
    public class Vector2IntNode : Node<Vector2Int>
    {
        private static bool IS_DIAGONAL = true;

        private Vector2IntNodeManager manager;
        private Vector2Int value;

        public byte Cost { get; set; }
        public bool Walkable { get; set; }

        public Vector2IntNode(Vector2IntNodeManager manager, Vector2Int value, byte cost, bool walkable)
        {
            this.manager = manager;
            this.value = value;
            this.Cost = cost;
            this.Walkable = walkable;
        }

        public IEnumerable<DirectedEdge<Vector2Int>> getTraversableNeighbors()
        {
            Vector2IntNeighbor neighbor;
            foreach (ZipEntry<Vector2Int, Vector2Int> zipEntry in EnumerableExtensions.Zip<Vector2Int, Vector2Int>(CardinalVector2Ints.Cardinal, CardinalVector2Ints.Intercardinal))
            {
                neighbor = getTraversableNeighbor(zipEntry.Value1, !IS_DIAGONAL);
                if (neighbor != null)
                {
                    yield return neighbor;
                }
                neighbor = getTraversableNeighbor(zipEntry.Value2, IS_DIAGONAL);
                if (neighbor != null)
                {
                    yield return neighbor;
                }
            }
        }

        private Vector2IntNeighbor getTraversableNeighbor(Vector2Int direction, bool isDiagonal)
        {
            Vector2Int neighborValue = getValue() + direction;
            if (manager.Contains(neighborValue) && manager[neighborValue].Walkable)
            {
                Vector2IntNode neighbor = manager[neighborValue];
                return new Vector2IntNeighbor(neighbor, calculateMoveCost(neighbor.Cost , isDiagonal));
            }

            return null;
        }

        private int calculateMoveCost(int nodeCost, bool isDiagonal)
        {
            if (isDiagonal)
            {
                return Vector2IntHeuristicManager.timesDiagonal(nodeCost);
            }
            else
            {
                return nodeCost;
            }
        }

        public Vector2Int getValue()
        {
            return value;
        }

        public override int GetHashCode()
        {
            return value.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is Vector2IntNode)
            {
                return value.Equals(((Vector2IntNode)obj).value);
            } else
            {
                return false;
            }
        }
    }
}

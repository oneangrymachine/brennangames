﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Common.Collections;
using Brennan.Games.Common.Numeric;
using Brennan.Games.PathFinding.AStar;

namespace Brennan.Games.PathFinding.Vector2IntPathFinder
{
    public class Vector2IntAStar : ShortestPathFinder<Vector2Int>, Vector2IntNodeCostManager
    {
        private Vector2IntNodeManager nodeManager;

        public Vector2IntAStar(Vector2Int size)
        {
            nodeManager = new Vector2IntNodeManager(this, size);
        }

        public void OccupySpace(Vector2Int space)
        {
            SetWalkable(space, false);
        }

        public void UnoccupySpace(Vector2Int space)
        {
            SetWalkable(space, true);
        }

        public void SetWalkable(Vector2Int space, bool isWalkable)
        {
            if (nodeManager.Contains(space))
            {
                nodeManager[space].Walkable = isWalkable;
            }
        }

        public IEnumerable<Vector2Int> FindShortestPath(Vector2Int start, Vector2Int goal)
        {
            if (start == null || goal == null)
            {
                return null;
            }
            if (!nodeManager.Contains(start) || !nodeManager.Contains(goal))
            {
                return null;
            }
            HeuristicManager<Vector2Int> hueristicManager = new Vector2IntHeuristicManager(goal);
            return new AStarRequest<Vector2Int>(hueristicManager, nodeManager[start]).FindShortestPath();
        }

        public byte getCost(Vector2Int point)
        {
            return 1;
        }
    }
}
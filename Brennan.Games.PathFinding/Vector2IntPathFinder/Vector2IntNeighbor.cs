﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.PathFinding.AStar;
using Brennan.Games.Common.Numeric;

namespace Brennan.Games.PathFinding.Vector2IntPathFinder
{
    class Vector2IntNeighbor : DirectedEdge<Vector2Int>
    {
        private Node<Vector2Int> node;
        private int cost;

        public Vector2IntNeighbor(Node<Vector2Int> node, int cost)
        {
            this.node = node;
            this.cost = cost;
        }

        public Node<Vector2Int> getNode()
        {
            return node;
        }

        public int getCost()
        {
            return cost;
        }
    }
}

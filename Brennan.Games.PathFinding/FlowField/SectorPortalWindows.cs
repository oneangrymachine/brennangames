﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.PathFinding.FlowField.PortalWindows;
using Brennan.Games.PathFinding.FlowField.Edges;
using Brennan.Games.Common.Numeric;
using Brennan.Games.Common.Collections;

namespace Brennan.Games.PathFinding.FlowField
{
    public class SectorPortalWindows : IEnumerable<PortalWindow>
    {
        public List<PortalWindow> Top { get; private set; }
        public List<PortalWindow> Bottom { get; private set; }
        public List<PortalWindow> Left { get; private set; }
        public List<PortalWindow> Right { get; private set; }

        private Dictionary<SectorEdge, List<PortalWindow>> edgePortalWindowMap;
        private ChainedEnumerable<PortalWindow> chainedEnumerable;

        public SectorPortalWindows(Vector2Int size, SectorEdges sectorEdges)
        {
            Top = new List<PortalWindow>(size.X / 2);
            Bottom = new List<PortalWindow>(size.X / 2);
            Left = new List<PortalWindow>(size.Y / 2);
            Right = new List<PortalWindow>(size.Y / 2);
            setupEdgeMap(sectorEdges);
            chainedEnumerable = new ChainedEnumerable<PortalWindow>(Top, Bottom, Left, Right);
        }

        private void setupEdgeMap(SectorEdges edges)
        {
            edgePortalWindowMap = new Dictionary<SectorEdge, List<PortalWindow>>(4);
            edgePortalWindowMap.Add(edges.TopEdge, Top);
            edgePortalWindowMap.Add(edges.BottomEdge, Bottom);
            edgePortalWindowMap.Add(edges.LeftEdge, Left);
            edgePortalWindowMap.Add(edges.RightEdge, Right);
        }

        public List<PortalWindow> GetPortalWindowList(SectorEdge sectorEdge)
        {
            return edgePortalWindowMap[sectorEdge];
        }

        public void Add(PortalWindow portalWindow, SectorEdge sectorEdge)
        {
            edgePortalWindowMap[sectorEdge].Add(portalWindow);
        }

        public PortalWindow[] GetPortalWindows(Vector2Int cellSectorCordinate)
        {
            PortalWindow[] windows = new PortalWindow[2];
            PortalWindow window = null;
            int index = 0;

            foreach (KeyValuePair<SectorEdge, List<PortalWindow>> keyValue in edgePortalWindowMap)
            {
                if (keyValue.Key.ContainsCell(cellSectorCordinate))
                {
                    window = FindPortalWindow(keyValue.Value, cellSectorCordinate);
                    if (window != null)
                    {
                        windows[index] = window;
                    }
                    index++;
                }
            }

            return windows;
        }

        public PortalWindow FindPortalWindow(List<PortalWindow> portalWindows, Vector2Int cellSectorCordinate)
        {
            foreach (PortalWindow pw in portalWindows)
            {
                if (cellSectorCordinate.WithinRangeInclusive(pw.Min, pw.Max))
                {
                    return pw;
                }
            }

            return null;
        }

        public void RemovePortalPair(SectorEdge sectorEdge, PortalWindow portalWindow)
        {
            Remove(sectorEdge, portalWindow);
            portalWindow.PortalPair.Node.Sector.Windows.Remove(sectorEdge.Opposite, portalWindow.PortalPair.Node);
        }

        private void Remove(SectorEdge sectorEdge, PortalWindow portalWindow)
        {
            GetPortalWindowList(sectorEdge).Remove(portalWindow);
        }

        public IEnumerator<PortalWindow> GetEnumerator()
        {
            return chainedEnumerable.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}

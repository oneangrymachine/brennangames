﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Common.Numeric;

namespace Brennan.Games.PathFinding.FlowField
{
    [Serializable()]
    public class IntegrationField
    {
        public IntegrationCell[,] IntegratedField { get; private set;}

        public IntegrationField(int width, int height)
        {
            IntegratedField = new IntegrationCell[width, height];

            for (int h = 0; h < height; h++)
            {
                for (int w = 0; w < width; w++)
                {
                    IntegratedField[w, h] = new IntegrationCell(new Vector2Int(w, h), int.MaxValue, false);
                }
            }
        }
    }

    [Serializable()]
    public class IntegrationCell : IComparable
    {
        public IntegrationCell(Vector2Int sectorCellCordinate, int integratedCost, bool hasLineOfSite)
        {
            this.SectorCellCordinate = sectorCellCordinate;
            this.IntegratedCost = integratedCost;
            this.HasLineOfSite = hasLineOfSite;
            this.Integrated = false;
        }

        public Vector2Int SectorCellCordinate { get; set; }
        public int IntegratedCost { get; set; }
        public bool HasLineOfSite { get; set; }
        public bool Integrated { get; set; }

        public int CompareTo(object obj)
        {
            return IntegratedCost.CompareTo(((IntegrationCell)obj).IntegratedCost);
        }

        public override int GetHashCode()
        {
            return SectorCellCordinate.GetHashCode();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

using Brennan.Games.Common.Numeric;
using Brennan.Games.PathFinding.FlowField;
using Brennan.Games.PathFinding.AStar;
using Sys;

namespace Brennan.Games.PathFinding.FlowField.PortalWindows
{
    public class PortalWindow : Node<PortalWindow>
    {
        public Sector Sector { get; private set; }
        public PortalLink PortalPair { get; set; }
        public List<DirectedEdge<PortalWindow>> SameSectorPortals { get; private set; }

        public Vector2Int Min { get; private set; }
        public Vector2Int Max { get; private set; }

        public bool Empty
        {
            get
            {
                return (Min.X > Max.X) || (Min.Y > Max.Y);
            }
        }

        public PortalWindow(Sector sector, Vector2Int min, Vector2Int max)
        {
            Sector = sector;
            Min = min;
            Max = max;
            SameSectorPortals = new List<DirectedEdge<PortalWindow>>();
        }

        public void RemoveMin()
        {
            Min = DecrementRange(1, Min);
        }

        public void RemoveMax()
        {
            Max = DecrementRange(-1, Max);
        }

        private Vector2Int DecrementRange(int delta, Vector2Int point)
        {
            if (Min.X == Max.X)
            {
                return new Vector2Int(point.X, point.Y + delta);
            }
            else if (Min.Y == Max.Y)
            {
                return new Vector2Int(point.X + delta, point.Y);
            }

            return Vector2Int.Zero;
        }

        public IEnumerable<DirectedEdge<PortalWindow>> getTraversableNeighbors()
        {
            return SameSectorPortals;
        }

        public PortalWindow getValue()
        {
            return this;
        }

        public override bool Equals(object obj)
        {
            if(obj is PortalWindow)
            {
                PortalWindow other = (PortalWindow) obj;
                if (Sector.SectorCordinate.Equals(other.Sector.SectorCordinate))
                {
                    if (Min.Equals(other.Min) && Max.Equals(other.Max))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public override int GetHashCode()
        {
            return (new Tuple<Vector2Int, Vector2Int, Vector2Int>(Sector.SectorCordinate, Min, Max)).GetHashCode();
        }
    }
}

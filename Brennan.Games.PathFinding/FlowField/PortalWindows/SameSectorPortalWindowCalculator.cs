﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Common.Numeric;
using Brennan.Games.PathFinding.AStar;
using Brennan.Games.PathFinding.Vector2IntPathFinder;

namespace Brennan.Games.PathFinding.FlowField.PortalWindows
{
    class SameSectorPortalWindowCalculator : Vector2IntNodeCostManager
    {
        private Sector sector;
        private Vector2IntNodeManager nodeManager;

        public SameSectorPortalWindowCalculator(Sector sector)
        {
            this.sector = sector;
            this.nodeManager = new Vector2IntNodeManager(this, sector.Size);
        }

        public void calculate()
        {
            PortalWindow current;
            foreach (PortalWindow portalWindow in sector.Windows)
            {
                current = portalWindow;
                foreach (PortalWindow otherPortalWindow in sector.Windows)
                {
                    if (!current.Equals(otherPortalWindow))
                    {
                        CreateLinkIfPathExists(current, otherPortalWindow);
                    }
                }
            }
        }

        private void CreateLinkIfPathExists(PortalWindow a, PortalWindow b)
        {
            List<Vector2Int> path = GetShortestPath(a, b);
            if (path != null)
            {
                GetPathCost(path);
            }
        }

        private List<Vector2Int> GetShortestPath(PortalWindow a, PortalWindow b)
        {
            Vector2IntHeuristicManager heuristicManager = new Vector2IntHeuristicManager(GetGoal(b));
            AStarRequest<Vector2Int> request = new AStarRequest<Vector2Int>(heuristicManager, getStart(a));
            return request.FindShortestPath();
        }

        private Node<Vector2Int> getStart(PortalWindow window)
        {
            Vector2Int mid = window.Min + ((window.Max - window.Min) / 2);
            return nodeManager[mid];
        }

        private int GetPathCost(List<Vector2Int> path) 
        {
            int totalCost = 0;
            foreach (Vector2Int point in path)
            {
                totalCost += getCost(point);
            }
            return totalCost;
        }

        private Vector2Int GetGoal(PortalWindow portalWindow)
        {
            return portalWindow.Min + (portalWindow.Max - portalWindow.Min) / 2;
        }

        public byte getCost(Vector2Int point)
        {
            return sector.CostField.Costs[point];
        }
    }
}

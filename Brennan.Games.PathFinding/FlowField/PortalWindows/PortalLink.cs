﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.PathFinding.AStar;

namespace Brennan.Games.PathFinding.FlowField.PortalWindows
{
    public class PortalLink : DirectedEdge<PortalWindow>
    {
        public PortalWindow Node { get; private set; }
        public int Weight { get; set; }

        public PortalLink(PortalWindow node, int weight)
        {
            Node = node;
            Weight = weight;
        }

        public Node<PortalWindow> getNode()
        {
            return Node;
        }

        public int getCost()
        {
            return Weight;
        }
    }
}

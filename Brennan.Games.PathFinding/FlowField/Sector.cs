﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Common.Numeric;
using Brennan.Games.PathFinding.FlowField.Edges;
using Brennan.Games.PathFinding.FlowField.PortalWindows;

namespace Brennan.Games.PathFinding.FlowField
{
    public class Sector
    {
        public Vector2Int Size { get; private set; }
        public int Width { get { return Size.X; } }
        public int Height { get { return Size.Y; } }
        public CostField CostField { get; private set; }
        public SectorPortalWindows Windows { get; private set; }
        public Vector2Int SectorCordinate { get; private set; }

        private CostFieldSetter costFieldSetter;

        public Sector(int sectorSize, SectorEdges sectorEdges, CostField costField, Vector2Int sectorCordinate)
        {
            init(sectorSize, sectorEdges);
            SectorCordinate = sectorCordinate;
            CostField = costField;
            costFieldSetter = new CostFieldSetter(this);
        }

        private void init(int sectorSize, SectorEdges sectorEdges)
        {
            Size = new Vector2Int(sectorSize, sectorSize);
            Windows = new SectorPortalWindows(Size, sectorEdges);
        }

        #region Set Cost
        class CostFieldSetter
        {
            protected Sector sector;

            public CostFieldSetter(Sector sector)
            {
                this.sector = sector;
            }

            internal virtual void SetCost(Vector2Int cellSectorCordinate, byte newCost)
            {
                sector.CostField.SetCost(cellSectorCordinate, newCost);
            }
        }

        class ClearCostFieldSetter : CostFieldSetter
        {
            public ClearCostFieldSetter(Sector sector) : base(sector) { }

            internal override void SetCost(Vector2Int cellSectorCordinate, byte newCost)
            {
                sector.CostField = new CostField(sector.CostField);
                sector.costFieldSetter = new CostFieldSetter(sector);
                base.SetCost(cellSectorCordinate, newCost);
            }
        }

        public void SetCost(Vector2Int cellSectorCordinate, byte newCost) 
        {
            costFieldSetter.SetCost(cellSectorCordinate, newCost);
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Brennan.Games.PathFinding.FlowField.Edges
{
    public enum EdgeAxes
    {
        Horizontal,
        Vertical
    }
}

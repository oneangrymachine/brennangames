﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Common.Numeric;

namespace Brennan.Games.PathFinding.FlowField.Edges
{
    public class LeftSectorEdge : SectorEdgeBase, IEnumerable<Vector2Int>
    {
        override public EdgeAxes Axes { get { return EdgeAxes.Horizontal; } }

        public LeftSectorEdge(Vector2Int sectorSize) : base(sectorSize) { }

        override public bool ContainsCell(Common.Numeric.Vector2Int cellSectorCordinate)
        {
            return cellSectorCordinate.X == 0;
        }

        override public IEnumerator<Vector2Int> GetEnumerator()
        {
            for (int y = 0; y < sectorSize.Y; y++)
            {
                yield return new Vector2Int(0, y);
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}

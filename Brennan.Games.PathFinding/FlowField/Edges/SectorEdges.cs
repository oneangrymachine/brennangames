﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Common.Numeric;

namespace Brennan.Games.PathFinding.FlowField.Edges
{
    public class SectorEdges : IEnumerable<SectorEdge>
    {
        public TopSectorEdge TopEdge { get; private set; }
        public BottomSectorEdge BottomEdge { get; private set; }
        public LeftSectorEdge LeftEdge { get; private set; }
        public RightSectorEdge RightEdge { get; private set; }

        public SectorEdges(Vector2Int sectorSize)
        {
            TopEdge = new TopSectorEdge(sectorSize);
            BottomEdge = new BottomSectorEdge(sectorSize);
            LeftEdge = new LeftSectorEdge(sectorSize);
            RightEdge = new RightSectorEdge(sectorSize);
            TopEdge.Opposite = BottomEdge;
            BottomEdge.Opposite = TopEdge;
            LeftEdge.Opposite = RightEdge;
            RightEdge.Opposite = LeftEdge;
        }

        public IEnumerator<SectorEdge> GetEnumerator()
        {
            yield return TopEdge;
            yield return BottomEdge;
            yield return LeftEdge;
            yield return RightEdge;
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Common.Numeric;

namespace Brennan.Games.PathFinding.FlowField.Edges
{
    public class BottomSectorEdge : SectorEdgeBase, IEnumerable<Vector2Int>
    {
        override public EdgeAxes Axes { get { return EdgeAxes.Horizontal; } }

        public BottomSectorEdge(Vector2Int sectorSize) : base(sectorSize) { }

        override public bool ContainsCell(Vector2Int cellSectorCordinate)
        {
            return cellSectorCordinate.Y == 0;
        }

        override public IEnumerator<Vector2Int> GetEnumerator()
        {
            for (int x = 0; x < sectorSize.X; x++)
            {
                yield return new Vector2Int(x, 0);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Common.Numeric;

namespace Brennan.Games.PathFinding.FlowField.Edges
{
    public abstract class SectorEdgeBase : SectorEdge, IEnumerable<Vector2Int>
    {
        protected Vector2Int sectorSize;

        public SectorEdge Opposite { get; set; }

        public abstract EdgeAxes Axes { get; }

        public SectorEdgeBase(Vector2Int sectorSize)
        {
            this.sectorSize = sectorSize;
        }

        public abstract bool ContainsCell(Vector2Int cellSectorCordinate);

        public abstract IEnumerator<Vector2Int> GetEnumerator();

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}

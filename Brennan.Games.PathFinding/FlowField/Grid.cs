﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Common.Numeric;
using Brennan.Games.PathFinding.FlowField;
using Brennan.Games.PathFinding.FlowField.Edges;
using Brennan.Games.PathFinding.FlowField.PortalWindows;

namespace Brennan.Games.PathFinding.FlowField
{
    public class Grid : IEnumerable<Sector>
    {
        public int Width { get; private set; }
        public int Height { get; private set; }
        public int SectorSize { get; private set; }
        public Sector[,] Sectors { get; private set; }
        public Vector2Int SectorDimension { get; private set; }
        public SectorEdges SectorEdges { get; private set; }
        public CostField ClearCostField { get; private set; }

        private PortalWindowManager portalWindowManager;

        public Grid(int width, int height, int sectorSize)
        {
            Width = width;
            Height = height;
            SectorSize = sectorSize;
            CreateSectors();
            CreatePortalWindows();
        }

        private void CreateSectors()
        {
            int horizontalCount = IntExtensions.DivRoundUp(Width, SectorSize);
            int verticalCount = IntExtensions.DivRoundUp(Height, SectorSize);
            SectorDimension = new Vector2Int(horizontalCount, verticalCount);
            Sectors = new Sector[SectorDimension.X, SectorDimension.Y];
            ClearCostField = new ClearCostField(SectorSize, SectorSize);
            SectorEdges = new SectorEdges(new Vector2Int(SectorSize, SectorSize));

            foreach (Vector2Int sectorCordinate in SectorDimension)
            {
                Sectors[sectorCordinate.X, sectorCordinate.Y] = new Sector(SectorSize, SectorEdges, ClearCostField, sectorCordinate);
            }
        }

        private void CreatePortalWindows()
        {
            portalWindowManager = new PortalWindowManager(this);
        }

        public Sector GetSectorFromSectorCordinate(Vector2Int sectorCordinate)
        {
            if (sectorCordinate.IsPositive() && sectorCordinate.WithinRangeExclusive(SectorDimension))
            {
                return Sectors[sectorCordinate.X, sectorCordinate.Y]; 
            }
            else
            {
                return null;
            }
        }

        public Sector GetSectorFromCellGridCordinate(Vector2Int cellGridCordinate)
        {
            Vector2Int sectorCordinate = cellGridCordinate / SectorSize;
            return GetSectorFromSectorCordinate(sectorCordinate);
        }

        public void SetCellCost(Vector2Int cellGridCordinate, byte newCost)
        {
            //cellSectorCordinate is the cordinate of a cell with respect to it's sector
            //cellGridCordinate is the cordinate of a cell with respect to the full grid
            Sector sector = GetSectorFromCellGridCordinate(cellGridCordinate);
            Vector2Int cellSectorCordinate = GetSectorCellCordinate(cellGridCordinate);
            sector.SetCost(cellSectorCordinate, newCost);
            portalWindowManager.UpdateSectorPortalWindows(sector, cellSectorCordinate);
        }

        public Vector2Int GetSectorCellCordinate(Vector2Int cellGridCordinate)
        {
            return cellGridCordinate % SectorSize;
        }

        public IEnumerator<Sector> GetEnumerator()
        {
            foreach(Vector2Int sectorCordinate in SectorDimension) 
            {
                yield return Sectors[sectorCordinate.X, sectorCordinate.Y];
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using Brennan.Games.Common;
using Brennan.Games.Common.Numeric;
using Brennan.Games.Common.Collections.Heaps;

namespace Brennan.Games.PathFinding.TwoDAStar
{
    public class AStarGrid : ShortestPathFinder<Vector2Int>
    {
        //private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        internal const int StraighCost = 10; //1 times 10
        internal const int DiagonalCost = 14;//sqrt of 2 times 10 rounded

        private GridNode[,] nodes;

        private Neighbors neighbors;

        private int xMax;
        private int yMax;

        public AStarGrid(int xMax, int yMax)
        {
            if (xMax < 1 || yMax < 1)
            {
                throw new ArgumentException("Grid Range Must be positive");
            }

            this.xMax = xMax;
            this.yMax = yMax;

            InitializeCollections();
        }

        private void InitializeCollections() {
            //occupiedSpace = new HashSet<RTSPoint>();

            nodes = new GridNode[xMax, yMax];
            for (int x = 0; x < xMax; x++)
            {
                for (int y = 0; y < yMax; y++)
                {
                    nodes[x, y] = new GridNode(x, y);
                }
            }

            neighbors = new Neighbors();
        }

        public void OccupySpace(Vector2Int vect)
        {
            GetNode(vect.X, vect.Y).Walkable = false;
        }

        public void UnoccupySpace(Vector2Int vect)
        {
            GetNode(vect.X, vect.Y).Walkable = true;
        }

        public IEnumerable<Vector2Int> FindShortestPath(Vector2Int start, Vector2Int goal)
        {
            HasIndexPriorityQueue<GridNode> openSet = new HasIndexPriorityQueue<GridNode>();

            GridNode startNode = GetNode(start.X, start.Y);
            GridNode goalNode = GetNode(goal.X, goal.Y);

            //start has no parent - set it's direction to none
            startNode.DirectionFromParent = Direction.None;
            // Cost from start along best known path.
            startNode.GScore = 0;

            // Estimated total cost from start to goal
            startNode.FScore = startNode.GScore + HeuristicCostEstimate(startNode, goalNode);

            //add the start node to the open set
            openSet.Push(startNode);
            startNode.InOpenSet = true;

            GridNode current;
            GridNode neighborPoint;
            int cost;
            int tentativeGScore;
            //List<Neighbor> pointWithCosts;
            bool spaceOccupied;
            bool inClosedSet;
            bool inOpenSet;
            int hVal;

            while (openSet.Count > 0)
            {
                //note: since the loop will break when the openSet is empty, the Pop will not return null here
                current = openSet.Pop();
                current.InOpenSet = false;

                //if we reached the goal, reconstruct the path and return
                if (current.Equals(goalNode))
                {
                    //log.Debug("Finished Finding Shortest Path");
                    return ReconstructPath(goalNode);
                }

                //add current to closedset
                current.InClosedSet = true;

                //loop through each neighbor of current
                UpdateNeighbors(current);

                foreach(Neighbor neighbor in neighbors)
                {
                    neighborPoint = neighbor.Point;
                    //cost = AdjustCost(current, neighbor); //cost of moving from current to neighbor
                    cost = neighbor.Cost; //cost of moving from current to neighbor

                    spaceOccupied = !neighborPoint.Walkable;

                    //if the neighbor is not walkable, ignore it.
                    if (spaceOccupied)
                    {
                        continue;
                    }

                    inClosedSet = neighborPoint.InClosedSet;

                    tentativeGScore = current.GScore + cost;

                    if (inClosedSet && tentativeGScore >= neighborPoint.GScore)
                    {
                        continue;
                    }

                    inOpenSet = neighborPoint.InOpenSet;

                    if (!inOpenSet || tentativeGScore < neighborPoint.GScore)
                    {
                        neighborPoint.parent = current;
                        //set the direction we came from our parent
                        //neighborPoint.DirectionFromParent = neighbor.DirectionFromParent;
                        neighborPoint.GScore = tentativeGScore;

                        hVal = HeuristicCostEstimate(neighborPoint, goalNode);

                        neighborPoint.FScore = neighborPoint.GScore + hVal;

                        if (!inOpenSet)
                        {
                            openSet.Push(neighborPoint);
                            neighborPoint.InOpenSet = true;
                        }
                        else
                        {
                            //fscore changed, need to adjust heap
                            openSet.Update(neighborPoint);
                        }
                    }
                }
            }
            //log.Debug("Could not find Path");

            ResetNodes();
            return null;
        }

        private int HeuristicCostEstimate(GridNode a, GridNode b)
        {
            int xDiff = Math.Abs(a.X - b.X);
            int yDiff = Math.Abs(a.Y - b.Y);

            //estimated distance is move as far diagonal as possible to goal, then move in a horizontal or vertical line
            return (Math.Min(xDiff, yDiff) * DiagonalCost + Math.Abs(xDiff - yDiff) * StraighCost);
        }

        private List<Vector2Int> ReconstructPath(GridNode goal)
        {
            //log.Debug("ReconstructPath Called");
            List<Vector2Int> path = new List<Vector2Int>();

            GridNode child = goal;

            while (child != null)
            {
                path.Insert(0, new Vector2Int(child.X, child.Y));
                child = child.parent;
            }

            //get ready for next path search
            ResetNodes();

            return path;
        }

        private void UpdateNeighbors(GridNode point)
        {
            bool includeTopRow = point.Y < yMax - 1;
            bool includeBottomRow = point.Y > 0;

            bool includeRightColumn = point.X < xMax - 1;
            bool includeLeftColumn = point.X > 0;


            //top row
            neighbors.NorthWestNeighbor.Point = includeTopRow && includeLeftColumn ? GetNode(point.X - 1, point.Y + 1) : null;
            neighbors.NorthNeighbor.Point = includeTopRow ? GetNode(point.X, point.Y + 1) : null;
            neighbors.NorthEastNeighbor.Point = includeTopRow && includeRightColumn ? GetNode(point.X + 1, point.Y + 1) : null;
            //middle row
            neighbors.WestNeighbor.Point = includeLeftColumn ? GetNode(point.X - 1, point.Y) : null;
            neighbors.EastNeighbor.Point = includeRightColumn ? GetNode(point.X + 1, point.Y) : null;
            //bottom row
            neighbors.SouthWestNeighbor.Point = includeBottomRow && includeLeftColumn ? GetNode(point.X - 1, point.Y - 1) : null;
            neighbors.SouthNeighbor.Point = includeBottomRow ? GetNode(point.X, point.Y - 1) : null;
            neighbors.SouthEastNeighbor.Point = includeBottomRow && includeRightColumn ? GetNode(point.X + 1, point.Y - 1) : null;
        }

        private GridNode GetNode(int x, int y)
        {
            return nodes[x, y];
        }

        private void ResetNodes()
        {
            //log.Debug("ResetNodes called");
            GridNode point;
            for (int x = 0; x < xMax; x++)
            {
                for (int y = 0; y < yMax; y++)
                {
                    point = nodes[x, y];
                    point.InClosedSet = false;
                    point.InOpenSet = false;
                    point.parent = null;
                }
            }

            neighbors = new Neighbors();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Brennan.Games.PathFinding.TwoDAStar
{
    internal enum Direction
    {
        None,
        North,
        NorthEast,
        East,
        SouthEast,
        South,
        SouthWest,
        West,
        NorthWest
    }
}

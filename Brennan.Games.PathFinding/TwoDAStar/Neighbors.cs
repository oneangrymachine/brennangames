﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Brennan.Games.PathFinding.TwoDAStar
{
    internal class Neighbors : IEnumerable<Neighbor>
    {
        private List<Neighbor> neighborsList;

        internal Neighbor NorthNeighbor = new Neighbor(null, AStarGrid.StraighCost, Direction.North);
        internal Neighbor NorthEastNeighbor = new Neighbor(null, AStarGrid.DiagonalCost, Direction.NorthEast);
        internal Neighbor EastNeighbor = new Neighbor(null, AStarGrid.StraighCost, Direction.East);
        internal Neighbor SouthEastNeighbor = new Neighbor(null, AStarGrid.DiagonalCost, Direction.SouthEast);
        internal Neighbor SouthNeighbor = new Neighbor(null, AStarGrid.StraighCost, Direction.South);
        internal Neighbor SouthWestNeighbor = new Neighbor(null, AStarGrid.DiagonalCost, Direction.SouthWest);
        internal Neighbor WestNeighbor = new Neighbor(null, AStarGrid.StraighCost, Direction.West);
        internal Neighbor NorthWestNeighbor = new Neighbor(null, AStarGrid.DiagonalCost, Direction.NorthWest);

        internal Neighbors()
        {
            neighborsList = new List<Neighbor>();
            neighborsList.Add(NorthNeighbor);
            neighborsList.Add(NorthEastNeighbor);
            neighborsList.Add(EastNeighbor);
            neighborsList.Add(SouthEastNeighbor);
            neighborsList.Add(SouthNeighbor);
            neighborsList.Add(SouthWestNeighbor);
            neighborsList.Add(WestNeighbor);
            neighborsList.Add(NorthWestNeighbor);
        }

        #region IEnumerable interface
        IEnumerator<Neighbor> IEnumerable<Neighbor>.GetEnumerator()
        {
            return new SkipNullEnumerator(neighborsList.GetEnumerator());
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return new SkipNullEnumerator(neighborsList.GetEnumerator());
        }
        #endregion IEnumerable interface
    }

    /// <summary>
    /// An Enumerator that will skip any null objects
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class SkipNullEnumerator : IEnumerator<Neighbor>
    {
        IEnumerator<Neighbor> innerEnumerator;

        internal SkipNullEnumerator(IEnumerator<Neighbor> innerEnumerator)
        {
            this.innerEnumerator = innerEnumerator;
        }

        Neighbor IEnumerator<Neighbor>.Current
        {
            get { return innerEnumerator.Current; }
        }

        void IDisposable.Dispose()
        {
            innerEnumerator.Dispose();
        }

        object IEnumerator.Current
        {
            get { return innerEnumerator.Current; }
        }

        bool IEnumerator.MoveNext()
        {
            //skip any neighbors whose point is null
            bool movedNext;
            do
            {
                movedNext = innerEnumerator.MoveNext();
            } while (movedNext && innerEnumerator.Current.Point == null);

            return movedNext;
        }

        void IEnumerator.Reset()
        {
            innerEnumerator.Reset();
        }
    }
}

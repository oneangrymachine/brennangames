﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Brennan.Games.PathFinding.AStar
{
    public interface DirectedEdge<T>
    {
        Node<T> getNode();
        int getCost();
    }
}

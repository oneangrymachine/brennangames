﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Brennan.Games.PathFinding.AStar
{
    public class NodePathData<T> : IComparable<NodePathData<T>>
    {
        public bool IsInClosedSet { get; set; }
        public Node<T> Parent { get; set; }
        public int GScore { get; set; }
        public int FScore { get; set; }

        public NodePathData()
        {
            IsInClosedSet = false;
            GScore = 0;
            FScore = 0;
        }

        public int CompareTo(NodePathData<T> other)
        {
            return FScore.CompareTo(other.FScore);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using Brennan.Games.Common;
using Brennan.Games.Common.Numeric;
using Brennan.Games.Common.Collections.Heaps;

namespace Brennan.Games.PathFinding.AStar
{
    public class AStarRequest<T>
    {
        HeuristicManager<T> heuristicCostEstimator;
        Node<T> start;
        PriorityQueue<Node<T>> openSet;
        NodesPathData<T> nodesPathData;

        public AStarRequest(HeuristicManager<T> heuristicCostEstimator, Node<T> start)
        {
            init(heuristicCostEstimator, start);
        }

        private void init(HeuristicManager<T> heuristicCostEstimator, Node<T> start)
        {
            this.start = start;
            this.heuristicCostEstimator = heuristicCostEstimator;
            nodesPathData = new NodesPathData<T>();
            openSet = new PrioritySet<Node<T>>(nodesPathData);
        }

        public List<T> FindShortestPath()
        {
            NodePathData<T> startData;
            startData = nodesPathData[start];
            // Estimated total cost from start to goal
            startData.FScore = startData.GScore + heuristicCostEstimator.EstimateCost(start.getValue());
            //add the start node to the open set
            openSet.Push(start);

            Node<T> current;
            Node<T> neighborNode;
            NodePathData<T> currentData;
            NodePathData<T> neighborData;
            int cost;
            int tentativeGScore;
            bool inOpenSet;
            int hVal;

            while (openSet.Count > 0)
            {
                //note: since the loop will break when the openSet is empty, the Pop will not return null here
                current = openSet.Pop();
                currentData = nodesPathData[current];

                //if we reached the goal, reconstruct the path and return
                if (heuristicCostEstimator.ReachedGoal(current.getValue()))
                {
                    return ReconstructPath(current);
                }

                //add current to closedset
                currentData.IsInClosedSet = true;

                //loop through each neighbor of current
                foreach (DirectedEdge<T> neighbor in current.getTraversableNeighbors())
                {
                    neighborNode = neighbor.getNode();
                    neighborData = nodesPathData[neighborNode];
                    cost = neighbor.getCost(); //cost of moving from current to neighbor

                    tentativeGScore = currentData.GScore + cost;

                    if (neighborData.IsInClosedSet && tentativeGScore >= neighborData.GScore)
                    {
                        continue;
                    }

                    inOpenSet = openSet.Contains(neighborNode);

                    if (!inOpenSet || tentativeGScore < neighborData.GScore)
                    {
                        neighborData.Parent = current;
                        neighborData.GScore = tentativeGScore;

                        hVal = heuristicCostEstimator.EstimateCost(neighborNode.getValue());

                        neighborData.FScore = neighborData.GScore + hVal;

                        if (!inOpenSet)
                        {
                            openSet.Push(neighborNode);
                        }
                        else
                        {
                            //fscore changed, need to adjust heap
                            openSet.Update(neighborNode);
                        }
                    }
                }
            }
            return null;
        }

        private List<T> ReconstructPath(Node<T> current)
        {
            List<T> path = new List<T>();
            path.Add(this.heuristicCostEstimator.GetGoal(current.getValue()));
            Node<T> child = current;
            while (child != null)
            {
                path.Insert(0, child.getValue());
                child = nodesPathData[child].Parent;
            }
            return path;
        }
    }
}
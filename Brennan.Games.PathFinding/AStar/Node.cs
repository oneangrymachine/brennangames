﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Brennan.Games.PathFinding.AStar
{
    public interface Node<T>
    {
        IEnumerable<DirectedEdge<T>> getTraversableNeighbors();
        T getValue();
    }
}

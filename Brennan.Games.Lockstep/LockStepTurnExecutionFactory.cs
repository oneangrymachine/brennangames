﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Lockstep.Actions;

namespace Brennan.Games.Lockstep
{
    internal class LockStepTurnExecutionFactory
    {
        
        private GamePerformance gamePerformance;
        private GameEngine.OnGameFrameExecutingDelegate onGameFrame;

        internal LockStepTurnExecutionFactory(GamePerformance gamePerformance, GameEngine.OnGameFrameExecutingDelegate onGameFrame)
        {
            this.gamePerformance = gamePerformance;
            this.onGameFrame = onGameFrame;
        }

        public LockStepTurnExecution ZeroTurn()
        {
            LockStepTurnExecution lockStepTurn = new LockStepTurnExecution(onGameFrame);
            return lockStepTurn;
        }

        public LockStepTurnExecution CalculateNextLockStepTurnExecution(LockStepTurnExecution previousLockStepTurnExecution)
        {
            LockStepTurn lockStepTurn = previousLockStepTurnExecution.LockStepTurn.NextTurn();
            LockStepTurnPerformance lockStepTurnPerformance = CalculateNextLockStepTurnPerformance();
            LockStepTurnExecution nextLockStepTurnExecution = new LockStepTurnExecution(onGameFrame, lockStepTurn, lockStepTurnPerformance,
                                                                    previousLockStepTurnExecution.RemainingAccumilatedTimeSinceLastGameFrame);
            return nextLockStepTurnExecution;
        }

        public void RecordActionsPerformance(PlayerAction[] actions)
        {
            gamePerformance.RecordPerformance(actions);
        }

        private LockStepTurnPerformance CalculateNextLockStepTurnPerformance()
        {
            int lockstepTurnLength = (gamePerformance.getMaxNetworkAverage() * 2/*two round trips*/) + 1/*minimum of 1 ms*/;
            int gameFrameTurnLength = gamePerformance.getMaxRuntimeAverage();

            //lockstep turn has to be at least as long as one game frame
            if (gameFrameTurnLength > lockstepTurnLength)
            {
                lockstepTurnLength = gameFrameTurnLength;
            }

            int gameFramesPerLockstepTurn = lockstepTurnLength / gameFrameTurnLength;
            //if gameframe turn length does not evenly divide the lockstep turn, there is extra time left after the last
            //game frame. Add one to the game frame turn length so it will consume it and recalculate the Lockstep turn length
            if (lockstepTurnLength % gameFrameTurnLength > 0)
            {
                gameFrameTurnLength++;
                lockstepTurnLength = gameFramesPerLockstepTurn * gameFrameTurnLength;
            }

            int lockstepsPerSecond = (1000 / lockstepTurnLength);
            if (lockstepsPerSecond == 0) { lockstepsPerSecond = 1; } //minimum per second

            int gameFramesPerSecond = lockstepsPerSecond * gameFramesPerLockstepTurn;
            LockStepTurnPerformance lockStepTurnPerformance = new LockStepTurnPerformance(gameFramesPerLockstepTurn, gameFrameTurnLength, gameFramesPerSecond, lockstepsPerSecond);
            gamePerformance.logTurnPerformance(lockStepTurnPerformance);
            return lockStepTurnPerformance;
        }
    }
}

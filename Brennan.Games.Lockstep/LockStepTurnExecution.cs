﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Brennan.Games.Lockstep
{
    internal class LockStepTurnExecution
    {
        private static long INITIAL_GAME_FRAME_TURN_LENGTH = 50; //In Milliseconds

        internal LockStepTurn LockStepTurn { get; private set; }
        internal LockStepTurnPerformance LockStepTurnPerformance { get; private set; }
        internal long MaxGameFrameRuntimeWithinTurn { get; set; }
        internal int RemainingAccumilatedTimeSinceLastGameFrame { get; private set; }
        internal bool Finished
        {
            get
            {
                return currentGameFrame == LockStepTurnPerformance.GameFramesPerLockstepTurn;
            }
        }

        private int currentGameFrame = 0;
        private Stopwatch gameTurnSW = new Stopwatch();
        private GameEngine.OnGameFrameExecutingDelegate onGameFrame;

        internal LockStepTurnExecution(GameEngine.OnGameFrameExecutingDelegate onGameFrame)
        {
            LockStepTurn = new LockStepTurn();
            MaxGameFrameRuntimeWithinTurn = INITIAL_GAME_FRAME_TURN_LENGTH;
            RemainingAccumilatedTimeSinceLastGameFrame = 0;
        }

        internal LockStepTurnExecution(GameEngine.OnGameFrameExecutingDelegate onGameFrame, LockStepTurn lockStepTurn, LockStepTurnPerformance lockStepTurnPerformance, int accumilatedTime)
        {
            this.onGameFrame = onGameFrame;
            this.LockStepTurn = lockStepTurn;
            this.LockStepTurnPerformance = lockStepTurnPerformance;
            this.RemainingAccumilatedTimeSinceLastGameFrame = accumilatedTime;
            MaxGameFrameRuntimeWithinTurn = 0;
        }

        internal void Tick(float deltaTime)
        {
            //Basically same logic as FixedUpdate, but we can scale it by adjusting FrameLength
            RemainingAccumilatedTimeSinceLastGameFrame += Convert.ToInt32((deltaTime * 1000)); //convert sec to milliseconds

            //in case the FPS is too slow, we may need to update the game multiple times a frame
            while (RemainingAccumilatedTimeSinceLastGameFrame > LockStepTurnPerformance.GameFrameTurnLength)
            {
                GameFrameTurn(deltaTime, onGameFrame);
                RemainingAccumilatedTimeSinceLastGameFrame -= LockStepTurnPerformance.GameFrameTurnLength;
            }
        }

        private void GameFrameTurn(float deltaTime, GameEngine.OnGameFrameExecutingDelegate onGameFrame)
        {
            //start the stop watch to determine game frame runtime performance
            gameTurnSW.Start();

            //update game
            if (onGameFrame != null)
            {
                onGameFrame.Invoke(new GameFrameExecutingEventArgs(LockStepTurnPerformance.GameFramesPerSecond));
            }

            currentGameFrame++;

            //stop the stop watch, the gameframe turn is over
            gameTurnSW.Stop();
            //update only if it's larger - we will use the game frame that took the longest in this lockstep turn
            long runtime = Convert.ToInt32((deltaTime * 1000))/*deltaTime is in secounds, convert to milliseconds*/ + gameTurnSW.ElapsedMilliseconds;
            if (runtime > MaxGameFrameRuntimeWithinTurn)
            {
                MaxGameFrameRuntimeWithinTurn = runtime;
            }
            //clear for the next frame
            gameTurnSW.Reset();
        }
    }
}

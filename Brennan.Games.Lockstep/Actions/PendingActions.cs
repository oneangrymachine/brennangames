﻿using System;
using System.Collections.Generic;

namespace Brennan.Games.Lockstep.Actions
{
    public class PendingActions
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private PlayerAction[] CurrentActions;
        private PlayerAction[] NextActions;
        private PlayerAction[] NextNextActions;
        //incase other players advance to the next step and send their action before we advance a step
        private PlayerAction[] NextNextNextActions;

        private int currentActionsCount;
        private int nextActionsCount;
        private int nextNextActionsCount;
        private int nextNextNextActionsCount;
        private int numberOfPlayers;

        public PendingActions(int numberOfPlayers)
        {
            this.numberOfPlayers = numberOfPlayers;
            CurrentActions = new PlayerAction[numberOfPlayers];
            NextActions = new PlayerAction[numberOfPlayers];
            NextNextActions = new PlayerAction[numberOfPlayers];
            NextNextNextActions = new PlayerAction[numberOfPlayers];

            currentActionsCount = 0;
            nextActionsCount = 0;
            nextNextActionsCount = 0;
            nextNextNextActionsCount = 0;
        }

        public PlayerAction[] getCurrentActions()
        {
            return CurrentActions;
        }

        public void NextTurn()
        {
            //Finished processing this turns actions - clear it
            for (int i = 0; i < CurrentActions.Length; i++)
            {
                CurrentActions[i] = null;
            }
            PlayerAction[] swap = CurrentActions;

            //last turn's actions is now this turn's actions
            CurrentActions = NextActions;
            currentActionsCount = nextActionsCount;

            //last turn's next next actions is now this turn's next actions
            NextActions = NextNextActions;
            nextActionsCount = nextNextActionsCount;

            NextNextActions = NextNextNextActions;
            nextNextActionsCount = nextNextNextActionsCount;

            //set NextNextNextActions to the empty list
            NextNextNextActions = swap;
            nextNextNextActionsCount = 0;
        }

        public void AddAction(PlayerAction action, int playerID, LockStepTurn currentLockStepTurn, LockStepTurn actionsLockStepTurn)
        {
            //add action for processing later
            if (actionsLockStepTurn.IsOneBefore(currentLockStepTurn))
            {
                //if action is for next turn, add for processing 3 turns away
                if (NextNextNextActions[playerID] != null)
                {
                    //TODO: Error Handling
                    log.Debug("WARNING!!!! Recieved multiple actions for player " + playerID + " for " + actionsLockStepTurn);
                }
                NextNextNextActions[playerID] = action;
                nextNextNextActionsCount++;
            }
            else if (actionsLockStepTurn.Equals(currentLockStepTurn))
            {
                //if recieved action during our current turn
                //add for processing 2 turns away
                if (NextNextActions[playerID] != null)
                {
                    //TODO: Error Handling
                    log.Debug("WARNING!!!! Recieved multiple actions for player " + playerID + " for turn " + actionsLockStepTurn);
                }
                NextNextActions[playerID] = action;
                nextNextActionsCount++;
            }
            else if (actionsLockStepTurn.IsOneAfter(currentLockStepTurn))
            {
                //if recieved action for last turn
                //add for processing 1 turn away
                if (NextActions[playerID] != null)
                {
                    //TODO: Error Handling
                    log.Debug("WARNING!!!! Recieved multiple actions for player " + playerID + " for " + actionsLockStepTurn);
                }
                NextActions[playerID] = action;
                nextActionsCount++;
            }
            else
            {
                //TODO: Error Handling
                log.Debug("WARNING!!!! Unexpected lockstepID recieved : " + actionsLockStepTurn);
                return;
            }
        }

        public bool ReadyForNextTurn(LockStepTurn lockStepTurn)
        {
            if (nextNextActionsCount == numberOfPlayers)
            {
                //if this is the 2nd turn, check if all the actions sent out on the 1st turn have been recieved
                if (lockStepTurn.IsSecondTurn)
                {
                    return true;
                }

                //Check if all Actions that will be processed next turn have been recieved
                if (nextActionsCount == numberOfPlayers)
                {
                    return true;
                }
            }

            //if this is the 1st turn, no actions had the chance to be recieved yet
            if (lockStepTurn.IsFirstTurn)
            {
                return true;
            }
            //if none of the conditions have been met, return false
            return false;
        }

        public int[] WhosNotReady(LockStepTurn lockStepTurn)
        {
            if (nextNextActionsCount == numberOfPlayers)
            {
                //if this is the 2nd turn, check if all the actions sent out on the 1st turn have been recieved
                if (lockStepTurn.IsSecondTurn)
                {
                    return null;
                }

                //Check if all Actions that will be processed next turn have been recieved
                if (nextActionsCount == numberOfPlayers)
                {
                    return null;
                }
                else
                {
                    return WhosNotReady(NextActions, nextActionsCount);
                }

            }
            else if (lockStepTurn.IsFirstTurn)
            {
                //if this is the 1st turn, no actions had the chance to be recieved yet
                return null;
            }
            else
            {
                return WhosNotReady(NextNextActions, nextNextActionsCount);
            }
        }

        private int[] WhosNotReady(PlayerAction[] actions, int count)
        {
            if (count < numberOfPlayers)
            {
                int[] notReadyPlayers = new int[numberOfPlayers - count];

                int index = 0;
                for (int playerID = 0; playerID < numberOfPlayers; playerID++)
                {
                    if (actions[playerID] == null)
                    {
                        notReadyPlayers[index] = playerID;
                        index++;
                    }
                }

                return notReadyPlayers;
            }
            else
            {
                return null;
            }
        }
    }
}

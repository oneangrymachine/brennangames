﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Brennan.Games.Lockstep.Actions
{
    [Serializable]
    public class NoAction : PlayerAction
    {
        public override void ProcessAction() { }
    }
}

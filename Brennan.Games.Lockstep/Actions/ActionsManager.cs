﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Brennan.Games.Lockstep.Serialization;

namespace Brennan.Games.Lockstep.Actions
{
    internal class ActionsManager
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        private IActionMessenger actionMessenger;
        private Queue<PlayerAction> actionsToSend;
        private PendingActions pendingActions;
        private ConfirmedActions confirmedActions;
        private ActionProcessor actionProcessor;
        private LockStepTurnExecutionFactory lockStepTurnExecutionFactory;
        private int initialNetworkAverage;
        private int playerID;
        private LockStepTurnExecution currentLockStepTurnExecution;
        private long processActionsRunTime;

        internal ActionsManager(LockStepTurnExecutionFactory lockStepTurnExecutionFactory, IActionMessenger actionMessenger, int numberOfPlayers, int playerID, int initialNetworkAverage)
        {
            this.lockStepTurnExecutionFactory = lockStepTurnExecutionFactory;
            this.actionMessenger = actionMessenger;
            this.actionProcessor = new ActionProcessor(numberOfPlayers);
            this.initialNetworkAverage = initialNetworkAverage;
            this.playerID = playerID;
            actionsToSend = new Queue<PlayerAction>();
            pendingActions = new PendingActions(numberOfPlayers);
            confirmedActions = new ConfirmedActions(numberOfPlayers);
            log.Debug("Number of players is " + numberOfPlayers);
        }

        public void EnqueueActionToSend(PlayerAction action)
        {
            actionsToSend.Enqueue(action);
        }

        public void RecievePlayersAction(PlayerAction action, int playerID, LockStepTurn actionsLockStepTurn)
        {
            pendingActions.AddAction(action, playerID, currentLockStepTurnExecution.LockStepTurn, actionsLockStepTurn);
        }

        public void ConfirmAction(int confirmingPlayerID, LockStepTurn confirmedActionLockStepTurn)
        {
            confirmedActions.ConfirmAction(confirmingPlayerID, currentLockStepTurnExecution.LockStepTurn, confirmedActionLockStepTurn);
        }

        public bool IsReadyForNextLockStepTurn()
        {
            if (confirmedActions.ReadyForNextTurn(currentLockStepTurnExecution.LockStepTurn))
            {
                if (pendingActions.ReadyForNextTurn(currentLockStepTurnExecution.LockStepTurn))
                {
                    return true;
                }
                else
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("Have not recieved player(s) actions: ");
                    foreach (int i in pendingActions.WhosNotReady(currentLockStepTurnExecution.LockStepTurn))
                    {
                        sb.Append(i + ", ");
                    }
                    log.Debug(sb.ToString());
                }
            }
            else
            {
                log.Debug("ConfirmedActions not ready for next turn. ");
                int[] whosNotReady = confirmedActions.WhosNotConfirmed(currentLockStepTurnExecution.LockStepTurn);
                if (whosNotReady != null)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("Have not recieved confirmation from player(s): ");
                    foreach (int i in whosNotReady)
                    {
                        sb.Append(i + ", ");
                    }
                    log.Debug(sb.ToString());
                }
            }
            return false;
        }

        public LockStepTurnExecution CurrentLockStepTurnExecution()
        {
            return currentLockStepTurnExecution;
        }

        public LockStepTurnExecution AdvanceToNextLockStepTurnExecution(float deltaTime)
        {
            PlayerAction[] processedActions = ProcessActions();
            if (processedActions != null)
            {
                lockStepTurnExecutionFactory.RecordActionsPerformance(processedActions);
            }
            currentLockStepTurnExecution = lockStepTurnExecutionFactory.CalculateNextLockStepTurnExecution(currentLockStepTurnExecution);
            return currentLockStepTurnExecution;
        }

        private PlayerAction[] ProcessActions()
        {
            //move the confirmed actions to next turn
            confirmedActions.NextTurn();
            //move the pending actions to this turn
            pendingActions.NextTurn();
            sendPendingAction(currentLockStepTurnExecution.MaxGameFrameRuntimeWithinTurn + processActionsRunTime);
            if (currentLockStepTurnExecution.LockStepTurn.IsAfterSecondTurn)
            {
                processActionsRunTime = actionProcessor.TimeProcessActions(pendingActions.getCurrentActions());
                return pendingActions.getCurrentActions();
            }
            return null;
        }

        private void sendPendingAction(long gameFrameRuntime)
        {
            PlayerAction action = determineNextAction();
            if (currentLockStepTurnExecution.LockStepTurn.IsAfterSecondTurn)
            {
                action.NetworkAverage = confirmedActions.GetPriorTime();
            }
            else
            {
                action.NetworkAverage = initialNetworkAverage;
            }
            action.RuntimeAverage = Convert.ToInt32(gameFrameRuntime);

            //add action to our own list of actions to process
            RecievePlayersAction(action, playerID, currentLockStepTurnExecution.LockStepTurn);
            //start the confirmed action timer for network average
            confirmedActions.StartTimer();
            //confirm our own action
            ConfirmAction(playerID, currentLockStepTurnExecution.LockStepTurn);
            //send action to all other players
            actionMessenger.SendActionToAllOtherPlayers(currentLockStepTurnExecution.LockStepTurn, playerID, action);
            log.Debug("Sent " + (action.GetType().Name) + " action for " + currentLockStepTurnExecution.LockStepTurn);
        }

        private PlayerAction determineNextAction()
        {
            PlayerAction action = null;
            if (actionsToSend.Count > 0)
            {
                action = actionsToSend.Dequeue();
            }

            //if no action for this turn, send the NoAction action
            if (action == null)
            {
                action = new NoAction();
            }
            return action;
        }
    }
}

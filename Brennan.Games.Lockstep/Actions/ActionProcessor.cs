﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Brennan.Games.Lockstep.Actions
{
    public class ActionProcessor
    {
        private Stopwatch gameTurnSW;
        private int numberOfPlayers;
        private int indexToProcessFirst = 0; //used to rotate what player's action gets processed first

        public ActionProcessor(int numberOfPlayers)
        {
            this.numberOfPlayers = numberOfPlayers;
            gameTurnSW = new Stopwatch();
        }

        public long TimeProcessActions(PlayerAction[] currentActions)
        {
            //process action should be considered in runtime performance
            gameTurnSW.Reset();
            gameTurnSW.Start();

            //Rotate the order the player actions are processed so there is no advantage given to
            //any one player
            for (int player = indexToProcessFirst; player < currentActions.Length; player++)
            {
                currentActions[player].ProcessAction();
            }

            for (int player = 0; player < indexToProcessFirst; player++)
            {
                currentActions[player].ProcessAction();
            }

            indexToProcessFirst++;
            if (indexToProcessFirst >= numberOfPlayers)
            {
                indexToProcessFirst = 0;
            }

            //finished processing actions for this turn, stop the stopwatch
            gameTurnSW.Stop();
            return gameTurnSW.ElapsedMilliseconds;
        }
    }
}

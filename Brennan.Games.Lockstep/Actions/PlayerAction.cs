﻿using System;

namespace Brennan.Games.Lockstep.Actions
{
    [Serializable]
    public abstract class PlayerAction
    {
        public int NetworkAverage { get; set; }
        public int RuntimeAverage { get; set; }

        public virtual void ProcessAction() { }
    }
}

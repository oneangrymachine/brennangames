﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Brennan.Games.Lockstep.Actions
{
    public class ConfirmedActions
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private bool[] confirmedCurrent;
        private bool[] confirmedPrior;

        private int confirmedCurrentCount;
        private int confirmedPriorCount;

        //Stop watches used to adjust lockstep turn length
        private Stopwatch currentSW;
        private Stopwatch priorSW;

        private int numberOfPlayers;

        public ConfirmedActions(int numberOfPlayers)
        {
            this.numberOfPlayers = numberOfPlayers;
            confirmedCurrent = new bool[numberOfPlayers];
            confirmedPrior = new bool[numberOfPlayers];

            ResetArray(confirmedCurrent);
            ResetArray(confirmedPrior);

            confirmedCurrentCount = 0;
            confirmedPriorCount = 0;

            currentSW = new Stopwatch();
            priorSW = new Stopwatch();
        }

        public int GetPriorTime()
        {
            return ((int)priorSW.ElapsedMilliseconds);
        }

        public void StartTimer()
        {
            currentSW.Start();
        }

        public void NextTurn()
        {
            //clear prior actions
            ResetArray(confirmedPrior);

            bool[] swap = confirmedPrior;
            Stopwatch swapSW = priorSW;

            //last turns actions is now this turns prior actions
            confirmedPrior = confirmedCurrent;
            confirmedPriorCount = confirmedCurrentCount;
            priorSW = currentSW;

            //set this turns confirmation actions to the empty array
            confirmedCurrent = swap;
            confirmedCurrentCount = 0;
            currentSW = swapSW;
            currentSW.Reset();
        }

        public void ConfirmAction(int confirmingPlayerID, LockStepTurn currentLockStepTurn, LockStepTurn confirmedActionLockStepTurn)
        {
            if (confirmedActionLockStepTurn.Equals(currentLockStepTurn))
            {
                //if current turn, add to the current Turn Confirmation
                confirmedCurrent[confirmingPlayerID] = true;
                confirmedCurrentCount++;
                //if we recieved the last confirmation, stop timer
                //this gives us the length of the longest roundtrip message
                if (confirmedCurrentCount == numberOfPlayers)
                {
                    currentSW.Stop();
                }
            }
            else if (confirmedActionLockStepTurn.IsOneAfter(currentLockStepTurn))
            {
                //if confirmation for prior turn, add to the prior turn confirmation
                confirmedPrior[confirmingPlayerID] = true;
                confirmedPriorCount++;
                //if we recieved the last confirmation, stop timer
                //this gives us the length of the longest roundtrip message
                if (confirmedPriorCount == numberOfPlayers)
                {
                    priorSW.Stop();
                }
            }
            else
            {
                //TODO: Error Handling
                log.Debug("WARNING!!!! Unexpected lockstepID Confirmed : " + confirmedActionLockStepTurn + " from player: " + confirmingPlayerID);
            }
        }

        public bool ReadyForNextTurn(LockStepTurn lockStepTurn)
        {
            //check that the action that is going to be processed has been confirmed
            if (confirmedPriorCount == numberOfPlayers)
            {
                return true;
            }
            //if 2nd turn, check that the 1st turns action has been confirmed
            if (lockStepTurn.IsSecondTurn)
            {
                return confirmedCurrentCount == numberOfPlayers;
            }
            //no action has been sent out prior to the first turn
            if (lockStepTurn.IsFirstTurn)
            {
                return true;
            }
            //if none of the conditions have been met, return false
            return false;
        }

        public int[] WhosNotConfirmed(LockStepTurn lockStepTurn)
        {
            //check that the action that is going to be processed has been confirmed
            if (confirmedPriorCount == numberOfPlayers)
            {
                return null;
            }
            //if 2nd turn, check that the 1st turns action has been confirmed
            if (lockStepTurn.IsSecondTurn)
            {
                if (confirmedCurrentCount == numberOfPlayers)
                {
                    return null;
                }
                else
                {
                    return WhosNotConfirmed(confirmedCurrent, confirmedCurrentCount);
                }
            }
            //no action has been sent out prior to the first turn
            if (lockStepTurn.IsFirstTurn)
            {
                return null;
            }

            return WhosNotConfirmed(confirmedPrior, confirmedPriorCount);
        }

        /// <summary>
        /// Returns an array of player IDs of those players who have not
        /// confirmed are prior action.
        /// </summary>
        /// <returns>An array of not confirmed player IDs, or null if all players have confirmed</returns>
        private int[] WhosNotConfirmed(bool[] confirmed, int confirmedCount)
        {
            log.Debug("ConfirmedCount is " + confirmedCount + " and number of players is " + numberOfPlayers);
            if (confirmedCount < numberOfPlayers)
            {
                //the number of "not confirmed" is the number of players minus the number of "confirmed"
                int[] notConfirmed = new int[numberOfPlayers - confirmedCount];
                int count = 0;
                log.Debug("Number of not confirmed players is " + count);
                //loop through each player and see who has not confirmed
                for (int playerID = 0; playerID < numberOfPlayers; playerID++)
                {
                    if (!confirmed[playerID])
                    {
                        //add "not confirmed" player ID to the array
                        notConfirmed[count] = playerID;
                        count++;
                    }
                }

                return notConfirmed;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Sets every element of the boolean array to false
        /// </summary>
        /// <param name="a">The array to reset</param>
        private void ResetArray(bool[] a)
        {
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = false;
            }
        }
    }
}

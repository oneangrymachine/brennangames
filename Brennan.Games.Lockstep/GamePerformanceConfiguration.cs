﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Brennan.Games.Lockstep
{
    public class GamePerformanceConfiguration
    {
        private static readonly int INITIAL_NETWORK_AVERAGE = 200; //in Milliseconds
        private static readonly int MAX_NETWORK_AVERAGE = 300;
        private static readonly int MAX_RUNTIME_AVERAGE = 300;
        private static readonly int INITIAL_GAME_FRAME_TURN_LENGTH = 50; //in Milliseconds

        public int InitialNetworkAverage { get; private set; }
        public int MaxNetworkAverage { get; private set; }
        public int MaxRuntimeAverage { get; private set; }
        public int InitialGameFrameTurnLength { get; private set; }

        public GamePerformanceConfiguration ()
        {
            InitialNetworkAverage = INITIAL_NETWORK_AVERAGE;
            MaxNetworkAverage = MAX_NETWORK_AVERAGE;
            MaxRuntimeAverage = MAX_RUNTIME_AVERAGE;
            InitialGameFrameTurnLength = INITIAL_GAME_FRAME_TURN_LENGTH;
        }

        public GamePerformanceConfiguration(int initialNetworkAverage, int maxNetworkAverage, int maxRuntimeAverage, int initialGameFrameTurnLength)
        {
            InitialNetworkAverage = InitialNetworkAverage;
            MaxNetworkAverage = maxNetworkAverage;
            MaxRuntimeAverage = maxRuntimeAverage;
            InitialGameFrameTurnLength = initialGameFrameTurnLength;
        }
    }
}

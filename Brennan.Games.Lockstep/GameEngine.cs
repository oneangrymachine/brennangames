﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Brennan.Games.Lockstep.Actions;
using Brennan.Games.Lockstep.Serialization;

namespace Brennan.Games.Lockstep
{
    public class GameEngine : IGameEngine
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public event EventHandler<GameFrameExecutingEventArgs> GameFrameExecuting;

        /// <summary>
        /// Allows collaborators to raise the GameFrameExecuting event.
        /// </summary>
        /// <param name="gameFrameExecutingEventArgs"></param>
        internal delegate void OnGameFrameExecutingDelegate(GameFrameExecutingEventArgs gameFrameExecutingEventArgs);

        private int numberOfPlayers;
        private ActionsManager actionsManager;
        private GameLoop gameLoop;
        private LockStepTurnExecutionFactory lockStepTurnExecutionFactory;
        private IActionMessenger actionMessenger;

        public GameEngine(int numberOfPlayers, int myPlayerID, IActionMessenger actionMessenger)
        {
            log.Debug("Prep Game called. My PlayerID: " + myPlayerID);
            this.numberOfPlayers = numberOfPlayers;
            this.actionMessenger = actionMessenger;
            GamePerformanceConfiguration gamePerformanceConfiguration = new GamePerformanceConfiguration();
            GamePerformance gamePerformance = new GamePerformance(numberOfPlayers, gamePerformanceConfiguration);
            lockStepTurnExecutionFactory = new LockStepTurnExecutionFactory(gamePerformance, OnGameFrameExecuting);
            actionsManager = new ActionsManager(lockStepTurnExecutionFactory, actionMessenger, numberOfPlayers, myPlayerID, gamePerformanceConfiguration.InitialNetworkAverage);
            gameLoop = new GameLoop(actionsManager);
            
        }

        #region Public API
        public void EnqueueActionToSend(PlayerAction action)
        {
            actionsManager.EnqueueActionToSend(action);
        }

        public void RecievePlayersAction(PlayerAction action, int playerID, LockStepTurn actionsLockStepTurn)
        {
            actionsManager.RecievePlayersAction(action, Convert.ToInt32(playerID), actionsLockStepTurn);
        }

        public void ConfirmActionRecieved(int confirmingPlayerID, LockStepTurn confirmedActionLockStepTurn)
        {
            actionsManager.ConfirmAction(confirmingPlayerID, confirmedActionLockStepTurn);
        }

        public void UpdateGame(float deltaTime)
        {
            gameLoop.UpdateGame(deltaTime);
        }
        #endregion

        protected virtual void OnGameFrameExecuting(GameFrameExecutingEventArgs e)
        {
            EventHandler<GameFrameExecutingEventArgs> handler = GameFrameExecuting;

            if(handler != null)
            {
                handler(this, e);
            }
        }
    }
}

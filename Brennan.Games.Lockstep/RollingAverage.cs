﻿using System;

namespace Brennan.Games.Lockstep
{
    public class RollingAverage
    {
        public int[] currentValues; //used only for logging

        private int[] playerAverages;
        private int maxValueAllowed;
        public RollingAverage(int numofPlayers, int initValue, int maxValueAllowed)
        {
            this.maxValueAllowed = maxValueAllowed;
            playerAverages = new int[numofPlayers];
            currentValues = new int[numofPlayers];
            for (int i = 0; i < numofPlayers; i++)
            {
                playerAverages[i] = initValue;
                currentValues[i] = initValue;
            }
        }

        public void Add(int newValue, int playerID)
        {
            if (newValue > maxValueAllowed)
            {
                newValue = maxValueAllowed;
            }

            if (newValue > playerAverages[playerID])
            {
                //rise quickly
                //playerAverages[playerID] = (playerAverages[playerID] * (3) + newValue * (7)) / 10;
                playerAverages[playerID] = newValue;
            }
            else
            {
                //slowly fall down
                playerAverages[playerID] = (playerAverages[playerID] * (9) + newValue * (1)) / 10;
            }

            currentValues[playerID] = newValue;
        }

        public int GetMax()
        {
            int max = int.MinValue;
            foreach (int average in playerAverages)
            {
                if (average > max)
                {
                    max = average;
                }
            }

            return max;
        }
    }
}

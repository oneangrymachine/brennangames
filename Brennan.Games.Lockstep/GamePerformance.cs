﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Lockstep.Actions;

namespace Brennan.Games.Lockstep
{
    public class GamePerformance
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private RollingAverage networkAverage;
        private RollingAverage runtimeAverage;

        public GamePerformance(int numberOfPlayers, GamePerformanceConfiguration gamePerformanceConfiguration)
        {
            networkAverage = new RollingAverage(numberOfPlayers, gamePerformanceConfiguration.InitialNetworkAverage, gamePerformanceConfiguration.MaxNetworkAverage);
            runtimeAverage = new RollingAverage(numberOfPlayers, gamePerformanceConfiguration.InitialGameFrameTurnLength, gamePerformanceConfiguration.MaxRuntimeAverage);
        }

        public void RecordPerformance(PlayerAction[] actions)
        {
            for (int playerID = 0; playerID < actions.Length; playerID++)
            {
                networkAverage.Add(actions[playerID].NetworkAverage, playerID);
                runtimeAverage.Add(actions[playerID].RuntimeAverage, playerID);
            }
        }

        public int getMaxNetworkAverage()
        {
            return networkAverage.GetMax();
        }

        public int getMaxRuntimeAverage()
        {
            return runtimeAverage.GetMax();
        }

        internal void logTurnPerformance(LockStepTurnPerformance lockStepTurnPerformance)
        {
            /***Log performance***/
            //log.Debug ("GameFramesPerSecond is " + GameFramesPerSecond);
            StringBuilder performanceLog = new StringBuilder();
            //add each players current network value
            foreach (int val in networkAverage.currentValues)
            {
                performanceLog.Append(val + ", ");
            }
            //add the network average
            performanceLog.Append(networkAverage.GetMax() + ", ");
            //add each players current runtime value
            foreach (int val in runtimeAverage.currentValues)
            {
                performanceLog.Append(val + ", ");
            }
            //add the runtime average
            performanceLog.Append(runtimeAverage.GetMax() + ", ");

            //add other metrics
            performanceLog.Append(lockStepTurnPerformance.GameFramesPerSecond + ", ");
            performanceLog.Append(lockStepTurnPerformance.LockStepsPerSecond + ", ");
            performanceLog.Append(lockStepTurnPerformance.GameFramesPerLockstepTurn);

            log.Info(performanceLog.ToString());
        }

        private static void logGameFrameRateHeaders(int numberOfPlayers)
        {
            StringBuilder headers = new StringBuilder();
            for (int i = 0; i < numberOfPlayers; i++)
            {
                headers.Append("P" + i + " NetworkAverage, ");
            }
            headers.Append("MaxNetworkAverage, ");
            for (int i = 0; i < numberOfPlayers; i++)
            {
                headers.Append("P" + i + " RuntimeAverage, ");
            }
            headers.Append("MaxRuntimeAverage, ");
            headers.Append("GameFramesPerSecond, ");
            headers.Append("LockstepsPerSecond, ");
            headers.Append("GameFramesPerLockstepTurn");

            log.Info(headers.ToString());
        }
    }
}

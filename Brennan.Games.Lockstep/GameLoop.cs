﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Lockstep.Actions;

namespace Brennan.Games.Lockstep
{
    internal class GameLoop
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private ActionsManager actionsManager;
        private LockStepTurnExecution currentLockStepTurnExecution;
        private int accumilatedTime = 0; //the accumilated time in Milliseconds that have passed since the last time GameFrame was called

        internal GameLoop(ActionsManager actionsManager)
        {
            this.actionsManager = actionsManager;
            this.currentLockStepTurnExecution = actionsManager.CurrentLockStepTurnExecution();
        }

        internal void UpdateGame(float deltaTime)
        {
            log.Debug("Current " + currentLockStepTurnExecution.LockStepTurn);
            if (currentLockStepTurnExecution.Finished)
            {
                if (!actionsManager.IsReadyForNextLockStepTurn())
                {
                    log.Debug(currentLockStepTurnExecution.LockStepTurn + " is finished, however the turn cannot be advanced.");
                    //TODO: Game cannot advance, notify player after X amount of time has passed.
                    return;
                }
                else
                {
                    currentLockStepTurnExecution = actionsManager.AdvanceToNextLockStepTurnExecution(deltaTime);
                }
            }
            currentLockStepTurnExecution.Tick(deltaTime);
        }
    }
}

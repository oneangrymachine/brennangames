﻿using Brennan.Games.Lockstep.Actions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Brennan.Games.Lockstep
{
    public interface IGameEngine
    {
        event EventHandler<GameFrameExecutingEventArgs> GameFrameExecuting;
        void UpdateGame(float deltaTime);
        void EnqueueActionToSend(PlayerAction action);
        void RecievePlayersAction(PlayerAction action, int playerID, LockStepTurn actionsLockStepTurn);
        void ConfirmActionRecieved(int confirmingPlayerID, LockStepTurn confirmedActionLockStepTurn);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Brennan.Games.Lockstep
{
    [Serializable]
    public class LockStepTurn
    {
        private static readonly int FirstLockStepTurnID = 0;
        private int lockStepTurnID;

        public LockStepTurn()
        {
            this.lockStepTurnID = FirstLockStepTurnID;
        }

        public LockStepTurn(int lockStepTurnID)
        {
            this.lockStepTurnID = lockStepTurnID;
        }

        public bool IsFirstTurn { get { return lockStepTurnID == FirstLockStepTurnID; } }
        public bool IsSecondTurn { get { return lockStepTurnID == FirstLockStepTurnID + 1; } }
        public bool IsAfterSecondTurn { get { return lockStepTurnID > FirstLockStepTurnID + 1; } }

        public bool IsAfter(LockStepTurn lockStepTurn)
        {
            return lockStepTurnID > lockStepTurn.lockStepTurnID;
        }

        public bool IsOneBefore(LockStepTurn lockStepTurn)
        {
            return lockStepTurnID == lockStepTurn.lockStepTurnID + 1;
        }

        public bool IsOneAfter(LockStepTurn lockStepTurn)
        {
            return lockStepTurnID == lockStepTurn.lockStepTurnID - 1;
        }

        public LockStepTurn NextTurn()
        {
            return new LockStepTurn(lockStepTurnID + 1);
        }

        public override bool Equals(object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else {
                LockStepTurn other = obj as LockStepTurn;
                return lockStepTurnID == other.lockStepTurnID;
            }
        }

        public override int GetHashCode()
        {
            return lockStepTurnID.GetHashCode();
        }

        public override string ToString()
        {
            return String.Format("lockStepTurn: {0}", lockStepTurnID);
        }

        //TODO: Refactor so we don't need this
        public int GetID()
        {
            return lockStepTurnID;
        }
    }
}

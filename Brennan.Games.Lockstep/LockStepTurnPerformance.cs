﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Brennan.Games.Lockstep
{
    internal class LockStepTurnPerformance
    {
        public int GameFramesPerLockstepTurn { get; private set; }
        public int GameFrameTurnLength { get; private set; }
        public int GameFramesPerSecond { get; private set; }
        public int LockStepsPerSecond { get; private set; }

        public LockStepTurnPerformance(int gameFramesPerLockStepTurn, int gameFrameTurnLength, int gameFramesPerSecond, int lockStepsPerSecond)
        {
            GameFramesPerLockstepTurn = gameFramesPerLockStepTurn;
            GameFrameTurnLength = gameFrameTurnLength;
            GameFramesPerSecond = gameFramesPerSecond;
            LockStepsPerSecond = lockStepsPerSecond;
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Brennan.Games.Lockstep
{
    public class GameFrameExecutingEventArgs : EventArgs
    {
        public int GameFramesPerSecond { get; private set; }

        public GameFrameExecutingEventArgs(int gameFramesPerSecond)
        {
            GameFramesPerSecond = gameFramesPerSecond;
        }
    }
}

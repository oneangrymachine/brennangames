﻿using System;
using System.Collections.Generic;
using System.Text;
using Brennan.Games.Lockstep.Actions;

namespace Brennan.Games.Lockstep
{
    public interface IActionMessenger
    {
        void SendActionToAllOtherPlayers(LockStepTurn currentLockStepTurn, int sendingPlayerID, PlayerAction action);
    }
}
